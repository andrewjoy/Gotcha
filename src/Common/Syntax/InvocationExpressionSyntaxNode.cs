﻿using Gotcha.Core;

namespace Gotcha.Common.Syntax
{
    sealed public class InvocationExpressionSyntaxNode : ExpressionSyntaxNode
    {
        public enum ChildPosition
        {
            Method = 0,
            Arguments = 1
        }

        public MemberAccessExpressionSyntaxNode Method { get; } = null;

        public ArgumentListSyntaxNode Arguments { get; } = null;

        public InvocationExpressionSyntaxNode(MemberAccessExpressionSyntaxNode method, ArgumentListSyntaxNode arguments)
        {
            this.Method = method;
            this.Arguments = arguments;
        }

        public override int CountChildren()
        {
            return 2;
        }

        public override ISyntaxNode FindChildByPosition(int position)
        {
            switch (position)
            {
                case 0:
                    return this.Method;
                case 1:
                    return this.Arguments;
            }

            return null;
        }

        public ISyntaxNode FindChildByPosition(ChildPosition position)
        {
            return FindChildByPosition((int)position);
        }

        public override void Emit(CodeBuilder code)
        {
            this.Method.Emit(code);

            code.Append(" ");

            this.Arguments.Emit(code);
        }
    }
}
