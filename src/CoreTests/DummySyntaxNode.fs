﻿namespace Gotcha.CoreTests

open System
open System.Collections.Generic

open Gotcha.Core

type DummySyntaxNode (children:ISyntaxNode[]) as this =
    interface ISyntaxNode with
        member thisInterface.CountChildren () : int =
            this.CountChildren ()
            
        member thisInterface.FindChildByPosition (position:int) : ISyntaxNode = 
            this.FindChildByPosition position

        member thisInterface.FindChildByType (targetType:Type, searchStartPosition:int) : ISyntaxNode =
            this.FindChildByType (targetType, searchStartPosition)

        member thisInterface.FindChildByType<'NODE_TYPE when 'NODE_TYPE : not struct and 'NODE_TYPE :> ISyntaxNode> (searchStartPosition:int) : 'NODE_TYPE =
            this.FindChildByType<'NODE_TYPE> searchStartPosition

        member thisInterface.FindChildByPredicate (predicate:Func<ISyntaxNode, bool>, searchStartPosition:int) : ISyntaxNode =
            this.FindChildByPredicate (predicate, searchStartPosition)

        member thisInterface.FindChildrenByType (targetType:Type) : ISyntaxNode[] =
            this.FindChildrenByType targetType

        member thisInterface.FindChildrenByType<'NODE_TYPE when 'NODE_TYPE : not struct and 'NODE_TYPE :> ISyntaxNode> () : 'NODE_TYPE[] =
            this.FindChildrenByType<'NODE_TYPE> ()

        member thisInterface.FindChildrenByPredicate (predicate:Func<ISyntaxNode, bool>) : ISyntaxNode[] =
            this.FindChildrenByPredicate predicate

        member thisInterface.Emit(code:CodeBuilder) : unit =
            this.Emit code

    new () =
        DummySyntaxNode ([| |])

    member private this.Search (position:int, predicate:(ISyntaxNode -> bool)) = 
        if position >= children.Length then
            Unchecked.defaultof<ISyntaxNode>
        else
            if predicate children.[position] then
                children.[position]
            else
                this.Search (position + 1, predicate)

    member this.CountChildren () : int = children.Length
            
    member this.FindChildByPosition (position:int) : ISyntaxNode = 
        if position >= 0 && position < children.Length then
            children.[position]
        else
            Unchecked.defaultof<ISyntaxNode>

    member this.FindChildByType (targetType:Type, searchStartPosition:int) : ISyntaxNode =
        let searchPredicate (node:ISyntaxNode) : bool =
            if node.GetType () = targetType then
                true
            else
                false
                
        this.Search (searchStartPosition, searchPredicate)
    
    member this.FindChildByType (targetType:Type) : ISyntaxNode =
        this.FindChildByType (targetType, 0)

    member this.FindChildByType<'NODE_TYPE when 'NODE_TYPE : not struct and 'NODE_TYPE :> ISyntaxNode> (searchStartPosition:int) : 'NODE_TYPE =
        let searchPredicate (node:ISyntaxNode) : bool =
            if node :? 'NODE_TYPE then
                true
            else
                false
                
        this.Search (searchStartPosition, searchPredicate) :?> 'NODE_TYPE

    member this.FindChildByType<'NODE_TYPE when 'NODE_TYPE : not struct and 'NODE_TYPE :> ISyntaxNode> () : 'NODE_TYPE =
        this.FindChildByType<'NODE_TYPE> (0)

    member this.FindChildByPredicate (predicate:Func<ISyntaxNode, bool>, searchStartPosition:int) : ISyntaxNode =
        this.Search (searchStartPosition, fun (node:ISyntaxNode) -> predicate.Invoke node)

    member this.FindChildByPredicate (predicate:Func<ISyntaxNode, bool>) : ISyntaxNode =
        this.FindChildByPredicate (predicate, 0)

    member this.FindChildrenByType (targetType:Type) : ISyntaxNode[] =
        let returnValue:List<ISyntaxNode> = List<ISyntaxNode> ()

        for position = 0 to children.Length - 1 do
            if children.[position].GetType() = targetType then
                returnValue.Add children.[position]

        returnValue.ToArray ()

    member this.FindChildrenByType<'NODE_TYPE when 'NODE_TYPE : not struct and 'NODE_TYPE :> ISyntaxNode> () : 'NODE_TYPE[] =
        let returnValue:List<'NODE_TYPE> = List<'NODE_TYPE> ()

        for position = 0 to children.Length - 1 do
            match children.[position] with
            | :? 'NODE_TYPE as typedChild -> returnValue.Add typedChild
            | _ -> ()

        returnValue.ToArray ()

    member this.FindChildrenByPredicate (predicate:Func<ISyntaxNode, bool>) : ISyntaxNode[] =
        let returnValue:List<ISyntaxNode> = List<ISyntaxNode> ()

        for position = 0 to children.Length - 1 do
            if predicate.Invoke children.[position] then
                returnValue.Add children.[position]

        returnValue.ToArray ()

    member this.Emit(code:CodeBuilder) : unit =
        ()

type SecondaryDummySyntaxNode (children:ISyntaxNode[]) =
    inherit DummySyntaxNode (children)

    new () =
        SecondaryDummySyntaxNode ([| |])