﻿using Gotcha.Core;

namespace Gotcha.Common.Syntax
{
    abstract public class TokenSyntaxNode : SyntaxNode
    {
        public string Name { get; } = null;

        public TokenSyntaxNode(string name)
        {
            this.Name = name;
        }

        public override int CountChildren()
        {
            return 0;
        }

        public override ISyntaxNode FindChildByPosition(int position)
        {
            return null;
        }
    }
}
