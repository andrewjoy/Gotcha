﻿using Gotcha.Core;

namespace Gotcha.Common.Syntax
{
    sealed public class IdentifierTokenSyntaxNode : TokenSyntaxNode
    {
        public IdentifierTokenSyntaxNode(string name)
            : base(name)
        {
            
        }

        public override void Emit(CodeBuilder code)
        {
            code.Append("identifier:" + this.Name);
        }
    }
}
