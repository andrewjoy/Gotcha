﻿namespace Gotcha.Core

open System
open System.IO
open System.Runtime.Serialization
open System.Runtime.Serialization.Json

type SyntaxRuleGroup =
| Warning = 0
| Error = 1
| Optimisation = 2

type SyntaxRule = {
    Name:String;
    Description:String;
    Group:SyntaxRuleGroup;
    Patterns:string[];
}

type SyntaxRule with
    static member Extension with get () : string = ".gsr"

    static member Deserialize (stream:Stream) : SyntaxRule =
        let deserializer:DataContractJsonSerializer = DataContractJsonSerializer typeof<SyntaxRule>
        
        deserializer.ReadObject (stream) :?> SyntaxRule

    static member Deserialize (fileInfo:FileInfo) : SyntaxRule =
        let mutable checkedFileInfo:FileInfo = fileInfo

        if fileInfo.Extension.Length = 0 then
            checkedFileInfo <- FileInfo (fileInfo.FullName + SyntaxRule.Extension)
        
        use fileStream:FileStream = checkedFileInfo.OpenRead ()

        SyntaxRule.Deserialize fileStream

    static member Deserialize (filePath:string) : SyntaxRule =
        SyntaxRule.Deserialize (FileInfo filePath)

    member this.Serialize (stream:Stream) : unit =
        let serializer:DataContractJsonSerializer = DataContractJsonSerializer typeof<SyntaxRule>

        serializer.WriteObject (stream, this)

    member this.Serialize (fileInfo:FileInfo) : unit = 
        let mutable checkedFileInfo:FileInfo = fileInfo

        if fileInfo.Extension <> SyntaxRule.Extension then
            checkedFileInfo <- FileInfo (fileInfo.FullName + SyntaxRule.Extension)

        use fileStream:FileStream = checkedFileInfo.Create ()
        
        this.Serialize (fileStream)

    member this.Serialize (filePath:string) : unit =
        this.Serialize (FileInfo filePath)