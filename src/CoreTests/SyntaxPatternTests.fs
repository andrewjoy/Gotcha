﻿namespace Gotcha.CoreTests

open System

open Gotcha.Core

open NUnit.Framework

[<TestFixture>]
type SyntaxPatternMatcherTests () =
    [<Test>]
    static member MatchFront_CorrectlyMatches () =
        let patternMatcher:SyntaxPatternMatcher = SyntaxPatternMatcher ([| DummySyntaxNode () |])

        Assert.True (patternMatcher.MatchFront<SecondaryDummySyntaxNode> ()).IsNone
        Assert.True (patternMatcher.MatchFront<DummySyntaxNode> ()).IsSome
        Assert.True (patternMatcher.MatchFront<DummySyntaxNode> ()).IsNone
    
    [<Test>]
    static member MatchFrontPredicate_CorrectlyMatches () =
        let patternMatcher:SyntaxPatternMatcher = SyntaxPatternMatcher ([| DummySyntaxNode ([| DummySyntaxNode (); DummySyntaxNode () |]) |])

        let predicate:Predicate<ISyntaxNode> = Predicate<ISyntaxNode> (fun (node:ISyntaxNode) -> node.CountChildren () = 2)

        Assert.True (patternMatcher.MatchFront (Predicate<ISyntaxNode> (fun (node:ISyntaxNode) -> node.CountChildren () = 0))).IsNone
        Assert.True (patternMatcher.MatchFront (predicate)).IsSome
        Assert.True (patternMatcher.MatchFront (predicate)).IsNone
    
    [<Test>]
    static member MatchBack_CorrectlyMatches () =
        let patternMatcher:SyntaxPatternMatcher = SyntaxPatternMatcher ([| DummySyntaxNode () |])

        Assert.True (patternMatcher.MatchBack<SecondaryDummySyntaxNode> ()).IsNone
        Assert.True (patternMatcher.MatchBack<DummySyntaxNode> ()).IsSome
        Assert.True (patternMatcher.MatchBack<DummySyntaxNode> ()).IsNone
    
    [<Test>]
    static member MatchBackPredicate_CorrectlyMatches () =
        let patternMatcher:SyntaxPatternMatcher = SyntaxPatternMatcher ([| DummySyntaxNode ([| DummySyntaxNode (); DummySyntaxNode () |]) |])

        let predicate:Predicate<ISyntaxNode> = Predicate<ISyntaxNode> (fun (node:ISyntaxNode) -> node.CountChildren () = 2)

        Assert.True (patternMatcher.MatchBack (Predicate<ISyntaxNode> (fun (node:ISyntaxNode) -> node.CountChildren () = 0))).IsNone
        Assert.True (patternMatcher.MatchBack (predicate)).IsSome
        Assert.True (patternMatcher.MatchBack (predicate)).IsNone
    
    [<Test>]
    static member MatchFrontRange_CorrectlyMatches () =
        let patternMatcher:SyntaxPatternMatcher = SyntaxPatternMatcher ([| DummySyntaxNode (); DummySyntaxNode (); SecondaryDummySyntaxNode (); |])

        Assert.True (patternMatcher.MatchFrontRange<DummySyntaxNode> (0, 100)).IsSome
        Assert.True (patternMatcher.MatchFrontRange<SecondaryDummySyntaxNode> (2, 100)).IsNone
        Assert.True (patternMatcher.MatchFrontRange<SecondaryDummySyntaxNode> (0, 100)).IsSome
        Assert.True (patternMatcher.MatchFrontRange<DummySyntaxNode> (1, 100)).IsNone

    [<Test>]
    static member MatchFrontRangePredicate_CorrectlyMatches () =
        let patternMatcher:SyntaxPatternMatcher = SyntaxPatternMatcher ([| DummySyntaxNode (); DummySyntaxNode (); DummySyntaxNode ([| DummySyntaxNode (); DummySyntaxNode () |]); |])

        let predicate:Predicate<ISyntaxNode> = Predicate<ISyntaxNode> (fun (node:ISyntaxNode) -> node.CountChildren () = 2)

        Assert.True (patternMatcher.MatchFrontRange (0, 100, fun (node:ISyntaxNode) -> node.CountChildren () = 0)).IsSome
        Assert.True (patternMatcher.MatchFrontRange (1, 100, predicate)).IsSome
        Assert.True (patternMatcher.MatchFrontRange (1, 100, predicate)).IsNone