﻿using Gotcha.Core;

namespace Gotcha.Common.Syntax
{
    sealed public class MethodDeclarationSyntaxNode : DeclarationSyntaxNode
    {
        public ArgumentListSyntaxNode Arguments { get; } = null;

        public MethodDeclarationSyntaxNode(ModifierListSyntaxNode modifiers, IdentifierTokenSyntaxNode identifier, ArgumentListSyntaxNode arguments)
            : base(modifiers, identifier)
        {
            this.Arguments = arguments;
        }

        public override void Emit(CodeBuilder code)
        {
            base.Emit(code);

            code.Append(" ");

            Arguments.Emit(code);
        }
    }
}
