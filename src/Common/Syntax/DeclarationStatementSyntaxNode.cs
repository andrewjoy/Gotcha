﻿using Gotcha.Core;

namespace Gotcha.Common.Syntax
{
    sealed public class DeclarationStatementSyntaxNode : StatementSyntaxNode
    {
        public enum ChildPosition
        {
            Declaration = 0,
            Value = 1
        }

        public DeclarationSyntaxNode Declaration { get; } = null;

        public ISyntaxNode Value { get; } = null;

        public DeclarationStatementSyntaxNode(DeclarationSyntaxNode declaration, ISyntaxNode value)
        {
            this.Declaration = declaration;
            this.Value = value;
        }

        public override int CountChildren()
        {
            return 2;
        }

        public override ISyntaxNode FindChildByPosition(int position)
        {
            switch (position)
            {
                case 0:
                    return this.Declaration;
                case 1:
                    return this.Value;
            }

            return null;
        }

        public ISyntaxNode FindChildByPosition(ChildPosition position)
        {
            return FindChildByPosition((int)position);
        }

        public override void Emit(CodeBuilder code)
        {
            this.Declaration.Emit(code);

            code.AppendLine(" with");
            code.AddIndent();

            this.Value.Emit(code);

            code.RemoveIndent();
        }
    }
}
