﻿namespace Gotcha.CoreTests.Query

open System

open Gotcha.Core
open Gotcha.Core.Query

open NUnit.Framework

type DummySyntaxQueryNode (children:ISyntaxNode[]) =
    inherit SyntaxQueryNode ()

    new () =
        DummySyntaxQueryNode [| |]

    override this.CountChildren () : int = 
        children.Length

    override this.FindChildByPosition (position:int) : ISyntaxNode = 
        if position >= 0 && position < children.Length then
            children.[position]
        else
            Unchecked.defaultof<ISyntaxNode>

    override this.Emit (code:CodeBuilder) : unit =
        ()

type DummyTokenSyntax () =
    inherit TokenSyntax ("")

[<TestFixture>]
type SyntaxQueryNodeTests () =
    [<Test>]
    static member Base_FindChildByType_FindsAtAllPositions () : unit = 
        SyntaxQueryNodeTests.Internal_FindChildByType_FindsAtAllPositions (DummySyntaxQueryNode [| DummySyntaxQueryNode (); DummySyntaxQueryNode (); DummySyntaxQueryNode () |])

    [<Test>]
    static member Base_FindChildByTypeGeneric_FindsAtAllPositions () : unit = 
        SyntaxQueryNodeTests.Internal_FindChildByTypeGeneric_FindsAtAllPositions (DummySyntaxQueryNode [| DummySyntaxQueryNode (); DummySyntaxQueryNode (); DummySyntaxQueryNode () |])

    [<Test>]
    static member Base_FindChildByPredicate_FindsAtAllPositions () : unit = 
        let children:ISyntaxNode[] = [| DummySyntaxQueryNode ([| DummySyntaxQueryNode () |]); DummySyntaxQueryNode ([| DummySyntaxQueryNode () |]); DummySyntaxQueryNode ([| DummySyntaxQueryNode () |]) |]
        SyntaxQueryNodeTests.Internal_FindChildByPredicate_FindsAtAllPositions (DummySyntaxQueryNode children, fun (node:ISyntaxNode) -> node.CountChildren () = 1)

    [<Test>]
    static member Base_FindChildByPredicate_IsAccurate () : unit =
        let children:ISyntaxNode[] = [| DummySyntaxQueryNode (); DummySyntaxQueryNode ([| DummySyntaxQueryNode (); DummySyntaxQueryNode () |]); DummySyntaxQueryNode () |]

        let predicate:Func<ISyntaxNode, bool> = Func<ISyntaxNode, bool> (fun (node:ISyntaxNode) -> node.CountChildren () = 2)

        Assert.True (predicate.Invoke ((DummySyntaxQueryNode children).FindChildByPredicate predicate))
    
    [<Test>]
    static member Base_FindChildrenByType_FindsAtAllPositions () : unit = 
        SyntaxQueryNodeTests.Internal_FindChildrenByType_FindsAtAllPositions (DummySyntaxQueryNode [| DummySyntaxQueryNode (); DummySyntaxQueryNode (); DummySyntaxQueryNode () |], typeof<DummySyntaxQueryNode>)

    [<Test>]
    static member Base_FindChildrenByTypeGeneric_FindsAtAllPositions () : unit = 
        SyntaxQueryNodeTests.Internal_FindChildrenByTypeGeneric_FindsAtAllPositions (DummySyntaxQueryNode [| DummySyntaxQueryNode (); DummySyntaxQueryNode (); DummySyntaxQueryNode () |])

    [<Test>]
    static member Base_FindChildrenByPredicate_FindsAtAllPositions () : unit = 
        let children:ISyntaxNode[] = [| DummySyntaxQueryNode ([| DummySyntaxQueryNode () |]); DummySyntaxQueryNode ([| DummySyntaxQueryNode () |]); DummySyntaxQueryNode ([| DummySyntaxQueryNode () |]) |]
        SyntaxQueryNodeTests.Internal_FindChildrenByPredicate_FindsAtAllPositions (DummySyntaxQueryNode children, fun (node:ISyntaxNode) -> node.CountChildren () = 1)

    [<Test>]
    static member Base_FindChildrenByPredicate_IsAccurate () : unit =
        let children:ISyntaxNode[] = [| DummySyntaxQueryNode (); DummySyntaxQueryNode ([| DummySyntaxQueryNode (); DummySyntaxQueryNode () |]); DummySyntaxQueryNode (); DummySyntaxQueryNode (); DummySyntaxQueryNode ([| DummySyntaxQueryNode (); DummySyntaxQueryNode () |]) |]

        let predicate:Func<ISyntaxNode, bool> = Func<ISyntaxNode, bool> (fun (node:ISyntaxNode) -> node.CountChildren () = 2)

        let foundChildren:ISyntaxNode[] = (DummySyntaxQueryNode children).FindChildrenByPredicate predicate

        Assert.AreEqual (2, foundChildren.Length)

        for foundChild in foundChildren do
            Assert.True (predicate.Invoke foundChild)

    [<Test>]
    static member Attributes_Contains_IsAccurate () : unit =
        let attributes:AttributesSyntax = AttributesSyntax ([| IdentifierTokenSyntax ("Foo"); IdentifierTokenSyntax ("Bar") |])

        Assert.True (attributes.Contains "Foo")
        Assert.False (attributes.Contains "Howdy")

    [<Test>]
    static member Attributes_FindChildrenByPosition_ReturnsNoNulls () : unit =
        SyntaxQueryNodeTests.Internal_FindChildByPosition_ReturnsNoNulls (AttributesSyntax ([| IdentifierTokenSyntax ("Foo"); IdentifierTokenSyntax ("Bar") |]))

    [<Test>]
    static member Collect_FindChildrenByPosition_ReturnsNoNulls () : unit =
        SyntaxQueryNodeTests.Internal_FindChildByPosition_ReturnsNoNulls (CollectStatementSyntax (AttributesSyntax ([| |]), IdentifierTokenSyntax ("System.Foo")))

    [<Test>]
    static member CompilationUnit_FindChildrenByPosition_ReturnsNoNulls () : unit =
        SyntaxQueryNodeTests.Internal_FindChildByPosition_ReturnsNoNulls (CompilationUnitSyntax ([| DummySyntaxQueryNode (); DummySyntaxQueryNode (); DummySyntaxQueryNode () |]))

    [<Test>]
    static member Filter_FindChildrenByPosition_ReturnsNoNulls () : unit =
        SyntaxQueryNodeTests.Internal_FindChildByPosition_ReturnsNoNulls (FilterStatementSyntax (AttributesSyntax ([| |]), IdentifierTokenSyntax ("Foo"), IdentifierTokenSyntax ("Bar")))

    [<Test>]
    static member Statement_FindChildrenByPosition_ReturnsNoNulls () : unit =
        SyntaxQueryNodeTests.Internal_FindChildByPosition_ReturnsNoNulls (CollectStatementSyntax (AttributesSyntax ([| |]), IdentifierTokenSyntax ("System.Foo")))

    [<Test>]
    static member Token_FindChildrenByPosition_ReturnsNoNulls () : unit =
        SyntaxQueryNodeTests.Internal_FindChildByPosition_ReturnsNoNulls (DummyTokenSyntax ())

    [<Test>]
    static member Unknown_FindChildrenByPosition_ReturnsNoNulls () : unit =
        SyntaxQueryNodeTests.Internal_FindChildByPosition_ReturnsNoNulls (UnknownSyntax ([| DummySyntaxQueryNode (); DummySyntaxQueryNode (); DummySyntaxQueryNode () |]))

    /// <summary>
    /// Tests whether a syntax node returns useful values at each child position through ISyntaxNode::FindChildByPosition.
    /// </summary>
    /// <param name="testNode">The node to test.</param>
    static member private Internal_FindChildByPosition_ReturnsNoNulls (testNode:ISyntaxNode) : unit =
        let childCount:int = testNode.CountChildren ()

        for childPosition = 0 to childCount - 1 do
            Assert.NotNull (testNode.FindChildByPosition childPosition)

    /// <summary>
    /// Tests whether each position is accessible by ISyntaxNode::FindChildByType.
    /// </summary>
    /// <param name="testNode">The node to test.</param>
    static member private Internal_FindChildByType_FindsAtAllPositions (testNode:ISyntaxNode) : unit =
        let childCount:int = testNode.CountChildren ()

        for childPosition = 0 to childCount - 1 do
            let childNode:ISyntaxNode = testNode.FindChildByPosition childPosition

            Assert.AreEqual (childNode, testNode.FindChildByType (childNode.GetType (), childPosition))

    /// <summary>
    /// Tests whether each position is accessible by ISyntaxNode::FindChildByType. In order for this test to work, all children of <paramref name="testNode"/> must be of type <typeparamref name="NODE_TYPE"/>.
    /// </summary>
    /// <typeparam name="NODE_TYPE">Type of node to find.</param>
    /// <param name="testNode">The node to test.</param>
    static member private Internal_FindChildByTypeGeneric_FindsAtAllPositions<'NODE_TYPE when 'NODE_TYPE : not struct and 'NODE_TYPE :> ISyntaxNode> (testNode:'NODE_TYPE) : unit =
        let childCount:int = testNode.CountChildren ()

        for childPosition = 0 to childCount - 1 do
            Assert.AreEqual (testNode.FindChildByPosition childPosition, testNode.FindChildByType<'NODE_TYPE> (childPosition))

    /// <summary>
    /// Tests whether each position is accessible by ISyntaxNode::FindChildByPredicate. In order for this test to work, all children of <paramref name="testNode"/> must satisfy <paramref name="predicate"/>.
    /// </summary>
    /// <param name="testNode">The node to test.</param>
    /// <param name="predicate">The predicate to find by.</param>
    static member private Internal_FindChildByPredicate_FindsAtAllPositions (testNode:ISyntaxNode, predicate:ISyntaxNode -> bool) : unit =
        let childCount:int = testNode.CountChildren ()

        for childPosition = 0 to childCount - 1 do
            Assert.AreEqual (testNode.FindChildByPosition childPosition, testNode.FindChildByPredicate (Func<ISyntaxNode, bool> predicate, childPosition))

    /// <summary>
    /// Tests whether each position is accessible by ISyntaxNode::FindChildrenByType. In order for this test to work, all children of <paramref name="testNode"/> must be of type <paramref name="testNodeChildrenType"/>.
    /// </summary>
    /// <param name="testNode">The node to test.</param>
    /// <param name="testNodeChildrenType">The type of all children of <paramref name="testNode"/>.</param>
    static member private Internal_FindChildrenByType_FindsAtAllPositions (testNode:ISyntaxNode, testNodeChildrenType:Type) : unit =
        let childCount:int = testNode.CountChildren ()

        let foundChildren:ISyntaxNode[] = testNode.FindChildrenByType testNodeChildrenType

        Assert.AreEqual (childCount, foundChildren.Length)

        for childPosition = 0 to childCount - 1 do
            Assert.AreEqual (testNode.FindChildByPosition childPosition, foundChildren.[childPosition])

    /// <summary>
    /// Tests whether each position is accessible by ISyntaxNode::FindChildrenByType. In order for this test to work, all children of <paramref name="testNode"/> must be of type <typeparamref name="NODE_TYPE"/>.
    /// </summary>
    /// <typeparam name="NODE_TYPE">Type of node to find.</param>
    /// <param name="testNode">The node to test.</param>
    static member private Internal_FindChildrenByTypeGeneric_FindsAtAllPositions<'NODE_TYPE when 'NODE_TYPE : not struct and 'NODE_TYPE :> ISyntaxNode> (testNode:'NODE_TYPE) : unit =
        let childCount:int = testNode.CountChildren ()

        let foundChildren:'NODE_TYPE[] = testNode.FindChildrenByType<'NODE_TYPE> ()

        Assert.AreEqual (childCount, foundChildren.Length)

        for childPosition = 0 to childCount - 1 do
            Assert.AreEqual (testNode.FindChildByPosition childPosition, foundChildren.[childPosition])

    /// <summary>
    /// Tests whether each position is accessible by ISyntaxNode::FindChildrenByPredicate. In order for this test to work, all children of <paramref name="testNode"/> must satisfy <paramref name="predicate"/>.
    /// </summary>
    /// <param name="testNode">The node to test.</param>
    /// <param name="predicate">The predicate to find by.</param>
    static member private Internal_FindChildrenByPredicate_FindsAtAllPositions (testNode:ISyntaxNode, predicate:ISyntaxNode -> bool) : unit =
        let childCount:int = testNode.CountChildren ()

        let foundChildren:ISyntaxNode[] = testNode.FindChildrenByPredicate (Func<ISyntaxNode, bool> predicate)

        Assert.AreEqual (childCount, foundChildren.Length)

        for childPosition = 0 to childCount - 1 do
            Assert.AreEqual (testNode.FindChildByPosition childPosition, foundChildren.[childPosition])

[<TestFixture>]
type SyntaxQueryParserTests () =
    [<Test>]
    static member ProcessTokens_WorksWithCollect () : unit =
        let lexer:SyntaxQueryLexer = SyntaxQueryLexer ()

        let tokens:SyntaxTokenHeirarchy = lexer.ProcessSource "COLLECT[] GotoStatementSyntax\n"

        let parser:SyntaxQueryParser = SyntaxQueryParser ()

        let rootNode:ISyntaxNode = parser.ProcessTokens tokens

        Assert.AreEqual (1, rootNode.CountChildren ())
        Assert.IsAssignableFrom<CollectStatementSyntax> (rootNode.FindChildByPosition 0)
    
    [<Test>]
    static member ProcessTokens_WorksWithFilter () : unit =
        let lexer:SyntaxQueryLexer = SyntaxQueryLexer ()

        let tokens:SyntaxTokenHeirarchy = lexer.ProcessSource "FILTER[] Foo Bar\n"

        let parser:SyntaxQueryParser = SyntaxQueryParser ()

        let rootNode:ISyntaxNode = parser.ProcessTokens tokens

        Assert.AreEqual (1, rootNode.CountChildren ())
        Assert.IsAssignableFrom<FilterStatementSyntax> (rootNode.FindChildByPosition 0)