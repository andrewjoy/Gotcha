﻿using Gotcha.Core;

namespace Gotcha.Common.Syntax
{
    sealed public class CompilationUnitSyntaxNode : SyntaxNode
    {
        private readonly ISyntaxNode[] children = null;

        public CompilationUnitSyntaxNode(ISyntaxNode[] children)
        {
            this.children = children;
        }

        public override int CountChildren()
        {
            return this.children.Length;
        }

        public override ISyntaxNode FindChildByPosition(int position)
        {
            if (position >= 0 && position < this.children.Length)
            {
                return this.children[position];
            }

            return null;
        }

        public override void Emit(CodeBuilder code)
        {
            code.AppendLine("Compilation Unit");

            code.AddIndent();

            for (int i = 0; i < this.children.Length; ++i)
            {
                this.children[i].Emit(code);

                code.AppendLine();
            }

            code.RemoveIndent();
        }
    }
}
