﻿namespace Gotcha.Core

open System
open System.Text

[<Sealed>]
type CodeBuilder () =
    let textBuilder:StringBuilder = StringBuilder ()

    let mutable indentString:string = ""
    let mutable isNewLine:bool = false

    member this.Append (text:string) : unit =
        if text.Length > 0 then
            if isNewLine = true then
                ignore (textBuilder.Append indentString)

                isNewLine <- false

            ignore (textBuilder.Append text)

    member this.AppendLine () : unit =
        if isNewLine = false then
            ignore (textBuilder.AppendLine ())

            isNewLine <- true

    member this.AppendLine (text:string) : unit = 
        this.Append (text)
        this.AppendLine ()

    member this.AddIndent () : unit =
        indentString <- indentString + "\t"

    member this.RemoveIndent () : unit =
        if indentString.Length > 0 then
            indentString <- indentString.Substring (0, indentString.Length - 1)

    override this.ToString () : string =
        textBuilder.ToString ()

[<AllowNullLiteral>]
type ISyntaxNode =
    /// <summary>
    /// Counts children at all positions under the node.
    /// </summary>
    /// <returns>Number of children of this node.</returns>
    abstract member CountChildren : unit -> int

    /// <summary>
    /// Retrieves the child node at <paramref name="position"/> (0 -> CountChildren - 1).
    /// </summary>
    /// <param name="position">Position to retrieve node from.</param>
    /// <returns>Node at <paramref name="position">.</returns>
    abstract member FindChildByPosition : position:int -> ISyntaxNode

    /// <summary>
    /// Retrieves the first child node of type <paramref name="targetType"/>.
    /// </summary>
    /// <param name="targetType">Type of node to find.</param>
    /// <param name="searchStartPosition">Position to start searching from.</param>
    /// <returns>First child node found that matches or derives from <paramref name="targetType"/>. Will search in ascending order by position.</returns>
    abstract member FindChildByType : targetType:Type * searchStartPosition:int -> ISyntaxNode

    /// <summary>
    /// Retrieves the first child node of type <typeparamref name="NODE_TYPE"/>.
    /// </summary>
    /// <typeparam name="NODE_TYPE">Type of node to find.</typeparam>
    /// <param name="searchStartPosition">Position to start searching from.</param>
    /// <returns>First child node found that matches or derives from <typeparamref name="NODE_TYPE"/>. Will search in ascending order by position.</returns>
    abstract member FindChildByType<'NODE_TYPE when 'NODE_TYPE : not struct and 'NODE_TYPE :> ISyntaxNode> : searchStartPosition:int -> 'NODE_TYPE

    /// <summary>
    /// Retrieves the first child node that satisfies <paramref name="predicate"/>.
    /// </summary>
    /// <param name="predicate">Predicate to test children against.</param>
    /// <returns>First child node found that satisfies <paramref name="predicate"/>.</returns>
    abstract member FindChildByPredicate : predicate:Func<ISyntaxNode, bool> * searchStartPosition:int -> ISyntaxNode

    /// <summary>
    /// Retrieves all child nodes of type <paramref name="targetType"/>.
    /// </summary>
    /// <param name="targetType">Type of node to find.</param>
    /// <returns>An array of child nodes found that match or derive from <paramref name="targetType"/>. Will search in ascending order by position.</returns>
    abstract member FindChildrenByType : targetType:Type -> ISyntaxNode[]

    /// <summary>
    /// Retrieves all child nodes of type <typeparamref name="NODE_TYPE"/>.
    /// </summary>
    /// <typeparam name="NODE_TYPE">Type of node to find.</typeparam>
    /// <returns>An array of child nodes found that match or derive from <typeparamref name="NODE_TYPE"/>. Will search in ascending order by position.</returns>
    abstract member FindChildrenByType<'NODE_TYPE when 'NODE_TYPE : not struct and 'NODE_TYPE :> ISyntaxNode> : unit -> 'NODE_TYPE[]

    /// <summary>
    /// Retrieves all child nodes that satisfy <paramref name="predicate"/>.
    /// </summary>
    /// <param name="predicate">Predicate to test children against.</param>
    /// <returns>An array of child nodes found that satisfy <paramref name="predicate"/>.</returns>
    abstract member FindChildrenByPredicate : predicate:Func<ISyntaxNode, bool> -> ISyntaxNode[]

    /// <summary>
    /// Emits code equivalent to this node.
    /// </summary>
    /// <param name="code">Emission target.</param>
    abstract member Emit : code:CodeBuilder -> unit