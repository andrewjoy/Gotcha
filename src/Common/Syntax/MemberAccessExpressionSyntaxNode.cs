﻿using Gotcha.Core;

namespace Gotcha.Common.Syntax
{
    sealed public class MemberAccessExpressionSyntaxNode : ExpressionSyntaxNode
    {
        private readonly IdentifierTokenSyntaxNode[] identifiers = null;

        public MemberAccessExpressionSyntaxNode(IdentifierTokenSyntaxNode[] identifiers)
        {
            this.identifiers = identifiers;
        }

        public override int CountChildren()
        {
            return this.identifiers.Length;
        }

        public override ISyntaxNode FindChildByPosition(int position)
        {
            if (position >= 0 && position < this.identifiers.Length)
            {
                return this.identifiers[position];
            }

            return null;
        }

        public override void Emit(CodeBuilder code)
        {
            for (int i = 0; i < this.identifiers.Length; ++i)
            {
                if (i > 0)
                {
                    code.Append("->");
                }

                this.identifiers[i].Emit(code);
            }
        }
    }
}
