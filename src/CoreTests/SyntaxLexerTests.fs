﻿namespace Gotcha.CoreTests

open System.Collections.Generic

open Gotcha.Core

open NUnit.Framework

[<TestFixture>]
type SyntaxLexerTests () =
    [<Test>]
    static member AddSplitterToken_OrdersCorrectly () : unit =
        let lexer:SyntaxLexer = SyntaxLexer ()

        lexer.AddSplitterLexemes [| "."; "("; "->"; ")"; "+"; "<-" |]

        let enumerator:IEnumerator<string> = lexer.SplitterLexemes.GetEnumerator ()

        Assert.True (enumerator.MoveNext ())
        Assert.AreEqual (enumerator.Current, "->")

        Assert.True (enumerator.MoveNext ())
        Assert.AreEqual (enumerator.Current, "<-")

        Assert.True (enumerator.MoveNext ())
        Assert.AreEqual (enumerator.Current, ".")

        Assert.True (enumerator.MoveNext ())
        Assert.AreEqual (enumerator.Current, "(")

        Assert.True (enumerator.MoveNext ())
        Assert.AreEqual (enumerator.Current, ")")

        Assert.True (enumerator.MoveNext ())
        Assert.AreEqual (enumerator.Current, "+")

    [<Test>]
    static member AddCombinerToken_OrdersCorrectly () : unit =
        let lexer:SyntaxLexer = SyntaxLexer ()

        lexer.AddCombinerLexemeRanges [| SyntaxLexemeRange (".", null, false); SyntaxLexemeRange ("(", null, false); SyntaxLexemeRange ("->", null, false); SyntaxLexemeRange (")", null, false); SyntaxLexemeRange ("+", null, false); SyntaxLexemeRange ("<-", null, false) |]

        let enumerator:IEnumerator<SyntaxLexemeRange> = lexer.CombinerLexemeRanges.GetEnumerator ()

        Assert.True (enumerator.MoveNext ())
        Assert.AreEqual (enumerator.Current.Start, "->")

        Assert.True (enumerator.MoveNext ())
        Assert.AreEqual (enumerator.Current.Start, "<-")

        Assert.True (enumerator.MoveNext ())
        Assert.AreEqual (enumerator.Current.Start, ".")

        Assert.True (enumerator.MoveNext ())
        Assert.AreEqual (enumerator.Current.Start, "(")

        Assert.True (enumerator.MoveNext ())
        Assert.AreEqual (enumerator.Current.Start, ")")

        Assert.True (enumerator.MoveNext ())
        Assert.AreEqual (enumerator.Current.Start, "+")

    [<Test>]
    static member SplitToken_OutputsSplitterTokens () : unit =
        let lexer:SyntaxLexer = SyntaxLexer ()

        lexer.AddSplitterLexemes [| "."; "("; ")"; "+" |]

        let result:string[] = lexer.SplitLexeme ("(++x).ToString()")

        Assert.AreEqual (9, result.Length)

        Assert.AreEqual ("(", result.[0])
        Assert.AreEqual ("+", result.[1])
        Assert.AreEqual ("+", result.[2])
        Assert.AreEqual ("x", result.[3])
        Assert.AreEqual (")", result.[4])
        Assert.AreEqual (".", result.[5])
        Assert.AreEqual ("ToString", result.[6])
        Assert.AreEqual ("(", result.[7])
        Assert.AreEqual (")", result.[8])

    [<Test>]
    static member TransformLexeme_AssociatesReservedTokens () : unit =
        let lexer:SyntaxLexer = SyntaxLexer ()

        lexer.AddReservedLexemes [| "int", SyntaxTokenKind.Keyword; "=", SyntaxTokenKind.Operator |]

        Assert.AreEqual (SyntaxTokenKind.Keyword, lexer.TransformLexeme("int").Kind)
        Assert.AreEqual (SyntaxTokenKind.IdentifierOrLiteral, lexer.TransformLexeme("x").Kind)
        Assert.AreEqual (SyntaxTokenKind.Operator, lexer.TransformLexeme("=").Kind)
        Assert.AreEqual (SyntaxTokenKind.IdentifierOrLiteral, lexer.TransformLexeme("5").Kind)

    [<Test>]
    static member GroupTokens_WorksForAllEdgeCases () : unit =
        let lexer:SyntaxLexer = SyntaxLexer ()

        lexer.AddLexemeSeparators [| ' ' |]
        lexer.AddGrouperLexemeRanges [| SyntaxLexemeRange ("((", "))", false); SyntaxLexemeRange ("(", ")", false); SyntaxLexemeRange ("<-", null, false); SyntaxLexemeRange (null, ";", false) |]

        // Standard layout, similar to ( )
        let mutable result:SyntaxTokenHeirarchy = lexer.ProcessSource("(( ))")

        match result with 
        | Leaf (token:SyntaxToken) ->
            Assert.Fail "Invalid Heirarchy Depth"
        | Parent (children:SyntaxTokenHeirarchy[]) ->
            Assert.AreEqual (2, children.Length)

            match children.[0] with
            | Leaf (token:SyntaxToken) ->
                Assert.AreEqual ("((", token.Lexeme)
            | Parent (children:SyntaxTokenHeirarchy[]) ->
                Assert.Fail "Invalid Heirarchy Depth"

            match children.[1] with
            | Leaf (token:SyntaxToken) ->
                Assert.AreEqual ("))", token.Lexeme)
            | Parent (children:SyntaxTokenHeirarchy[]) ->
                Assert.Fail "Invalid Heirarchy Depth"

        // Endless group beginning at end of token sequence
        result <- lexer.ProcessSource("<-")

        match result with 
        | Leaf (token:SyntaxToken) ->
            Assert.Fail "Invalid Heirarchy Depth"
        | Parent (children:SyntaxTokenHeirarchy[]) ->
            Assert.AreEqual (1, children.Length)

            match children.[0] with
            | Leaf (token:SyntaxToken) ->
                Assert.AreEqual ("<-", token.Lexeme)
            | Parent (children:SyntaxTokenHeirarchy[]) ->
                Assert.Fail "Invalid Heirarchy Depth"

        // Non-endless group beginning at end of token sequence
        result <- lexer.ProcessSource("(")

        match result with 
        | Leaf (token:SyntaxToken) ->
            Assert.Fail "Invalid Heirarchy Depth"
        | Parent (children:SyntaxTokenHeirarchy[]) ->
            Assert.AreEqual (1, children.Length)

            match children.[0] with
            | Leaf (token:SyntaxToken) ->
                Assert.AreEqual ("(", token.Lexeme)
            | Parent (children:SyntaxTokenHeirarchy[]) ->
                Assert.Fail "Invalid Heirarchy Depth"

    [<Test>]
    static member ProcessSource_CombinesTokens () : unit =
        let lexer:SyntaxLexer = SyntaxLexer ()

        lexer.AddLexemeSeparators [| ' ' |]
        lexer.AddCombinerLexemeRanges (SyntaxLexemeRange ("\"", "\"", false))

        let result:SyntaxTokenHeirarchy = lexer.ProcessSource("string value = \"Hello World\"")

        match result with 
        | Leaf (token:SyntaxToken) ->
            Assert.Fail "Invalid Heirarchy Depth"
        | Parent (children:SyntaxTokenHeirarchy[]) ->
            Assert.AreEqual (4, children.Length)

            match children.[0] with
            | Leaf (token:SyntaxToken) ->
                Assert.AreEqual ("string", token.Lexeme)
            | Parent (children:SyntaxTokenHeirarchy[]) ->
                Assert.Fail "Invalid Heirarchy Depth"

            match children.[1] with
            | Leaf (token:SyntaxToken) ->
                Assert.AreEqual ("value", token.Lexeme)
            | Parent (children:SyntaxTokenHeirarchy[]) ->
                Assert.Fail "Invalid Heirarchy Depth"

            match children.[2] with
            | Leaf (token:SyntaxToken) ->
                Assert.AreEqual ("=", token.Lexeme)
            | Parent (children:SyntaxTokenHeirarchy[]) ->
                Assert.Fail "Invalid Heirarchy Depth"

            match children.[3] with
            | Leaf (token:SyntaxToken) ->
                Assert.AreEqual ("\"Hello World\"", token.Lexeme)
            | Parent (children:SyntaxTokenHeirarchy[]) ->
                Assert.Fail "Invalid Heirarchy Depth"