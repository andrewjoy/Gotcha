﻿using Gotcha.Core;

namespace Gotcha.Common.Syntax
{
    sealed public class ArgumentListSyntaxNode : SyntaxNode
    {
        private readonly ArgumentSyntaxNode[] arguments = null;

        public ArgumentListSyntaxNode(ArgumentSyntaxNode[] arguments)
        {
            this.arguments = arguments;
        }

        public override int CountChildren()
        {
            return this.arguments.Length;
        }

        public override ISyntaxNode FindChildByPosition(int position)
        {
            if (position >= 0 && position < this.arguments.Length)
            {
                return this.arguments[position];
            }

            return null;
        }

        public override void Emit(CodeBuilder code)
        {
            code.Append("with arguments:[");

            for (int i = 0; i < this.arguments.Length; ++i)
            {
                if (i > 0)
                {
                    code.Append(" | ");
                }

                this.arguments[i].Emit(code);
            }

            code.Append("]");
        }
    }
}
