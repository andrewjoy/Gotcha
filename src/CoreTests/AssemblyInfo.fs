﻿namespace Gotcha.CoreTests.AssemblyInfo

open System.Reflection
open System.Runtime.InteropServices

[<assembly: AssemblyTitle("Gotcha.CoreTests")>]
[<assembly: AssemblyDescription("Tests for Gotcha.Core.")>]
[<assembly: AssemblyCulture("")>]

[<assembly: ComVisible(false)>]

[<assembly: Guid("1437901c-2bfb-41c1-a6b5-783ca967b84a")>]

[<assembly: AssemblyVersion("99.0.0.0")>]
[<assembly: AssemblyFileVersion("99.0.0.0")>]

do
    ()
