﻿namespace Gotcha.Core.Query

open System
open System.Reflection
open System.Collections.Generic

open Gotcha.Core

[<AbstractClass>]
type SyntaxQueryNode () as this =
    interface ISyntaxNode with
        member thisInterface.CountChildren () : int =
            this.CountChildren ()

        member thisInterface.FindChildByPosition (position:int) : ISyntaxNode =
            this.FindChildByPosition position

        member thisInterface.FindChildByType (targetType:Type, searchStartPosition:int) : ISyntaxNode =
            this.FindChildByType (targetType, searchStartPosition)

        member thisInterface.FindChildByType<'NODE_TYPE when 'NODE_TYPE : not struct and 'NODE_TYPE :> ISyntaxNode> (searchStartPosition:int) : 'NODE_TYPE =
            this.FindChildByType<'NODE_TYPE> searchStartPosition
            
        member thisInterface.FindChildByPredicate (predicate:Func<ISyntaxNode, bool>, searchStartPosition:int) : ISyntaxNode =
            this.FindChildByPredicate (predicate, searchStartPosition)

        member thisInterface.FindChildrenByType (targetType:Type) : ISyntaxNode[] =
            this.FindChildrenByType targetType

        member thisInterface.FindChildrenByType<'NODE_TYPE when 'NODE_TYPE : not struct and 'NODE_TYPE :> ISyntaxNode> () : 'NODE_TYPE[] =
            this.FindChildrenByType<'NODE_TYPE> ()

        member thisInterface.FindChildrenByPredicate (predicate:Func<ISyntaxNode, bool>) : ISyntaxNode[] =
            this.FindChildrenByPredicate predicate

        member thisInterface.Emit (code:CodeBuilder) : unit =
            this.Emit code

    abstract member CountChildren : unit -> int
    abstract member FindChildByPosition : position:int -> ISyntaxNode

    member this.FindChildByType (targetType:Type, searchStartPosition:int) : ISyntaxNode =
        let childCount:int = this.CountChildren ()

        let mutable currentSearchPosition:int = searchStartPosition
        let mutable returnValue:ISyntaxNode = Unchecked.defaultof<ISyntaxNode>

        while currentSearchPosition < childCount do
            let currentSearchChild:ISyntaxNode = this.FindChildByPosition currentSearchPosition

            if targetType.IsAssignableFrom (currentSearchChild.GetType ()) then
                returnValue <- currentSearchChild

                currentSearchPosition <- childCount - 1

            currentSearchPosition <- currentSearchPosition + 1

        returnValue

    member this.FindChildByType (targetType:Type) : ISyntaxNode =
        this.FindChildByType (targetType, 0)

    member this.FindChildByType<'NODE_TYPE when 'NODE_TYPE : not struct and 'NODE_TYPE :> ISyntaxNode> (searchStartPosition:int) : 'NODE_TYPE =
        let childCount:int = this.CountChildren ()

        let mutable currentSearchPosition:int = searchStartPosition
        let mutable returnValue:'NODE_TYPE = Unchecked.defaultof<'NODE_TYPE>

        while currentSearchPosition < childCount do
            let currentSearchChild:ISyntaxNode = this.FindChildByPosition currentSearchPosition

            if currentSearchChild :? 'NODE_TYPE then
                returnValue <- currentSearchChild :?> 'NODE_TYPE

                currentSearchPosition <- childCount - 1

            currentSearchPosition <- currentSearchPosition + 1

        returnValue

    member this.FindChildByType<'NODE_TYPE when 'NODE_TYPE : not struct and 'NODE_TYPE :> ISyntaxNode> () : 'NODE_TYPE =
        this.FindChildByType<'NODE_TYPE> 0
            
    member this.FindChildByPredicate (predicate:Func<ISyntaxNode, bool>, searchStartPosition:int) : ISyntaxNode =
        let childCount:int = this.CountChildren ()

        let mutable currentSearchPosition:int = searchStartPosition
        let mutable returnValue:ISyntaxNode = Unchecked.defaultof<ISyntaxNode>

        while currentSearchPosition < childCount do
            let currentSearchChild:ISyntaxNode = this.FindChildByPosition currentSearchPosition

            if predicate.Invoke currentSearchChild then
                returnValue <- currentSearchChild

                currentSearchPosition <- childCount - 1

            currentSearchPosition <- currentSearchPosition + 1

        returnValue

    member this.FindChildByPredicate (predicate:Func<ISyntaxNode, bool>) : ISyntaxNode =
        this.FindChildByPredicate (predicate, 0)

    member this.FindChildrenByType (targetType:Type) : ISyntaxNode[] =
        let childCount:int = this.CountChildren ()

        let returnValue:List<ISyntaxNode> = List<ISyntaxNode> (childCount)

        for childPosition = 0 to childCount - 1 do
            let child:ISyntaxNode = this.FindChildByPosition childPosition

            if targetType.IsAssignableFrom (child.GetType ()) then
                returnValue.Add child

        returnValue.ToArray ()

    member this.FindChildrenByType<'NODE_TYPE when 'NODE_TYPE : not struct and 'NODE_TYPE :> ISyntaxNode> () : 'NODE_TYPE[] =
        let childCount:int = this.CountChildren ()

        let returnValue:List<'NODE_TYPE> = List<'NODE_TYPE> (childCount)

        for childPosition = 0 to childCount - 1 do
            let child:ISyntaxNode = this.FindChildByPosition childPosition

            if child :? 'NODE_TYPE then
                returnValue.Add (child :?> 'NODE_TYPE)

        returnValue.ToArray ()

    member this.FindChildrenByPredicate (predicate:Func<ISyntaxNode, bool>) : ISyntaxNode[] =
        let childCount:int = this.CountChildren ()

        let returnValue:List<ISyntaxNode> = List<ISyntaxNode> (childCount)

        for childPosition = 0 to childCount - 1 do
            let child:ISyntaxNode = this.FindChildByPosition childPosition

            if predicate.Invoke child then
                returnValue.Add child

        returnValue.ToArray ()

    abstract member Emit : code:CodeBuilder -> unit

[<Sealed>]
type UnknownSyntax (children:ISyntaxNode[]) =
    inherit SyntaxQueryNode ()

    override this.CountChildren () : int = 
        children.Length

    override this.FindChildByPosition (position:int) : ISyntaxNode = 
        if position >= 0 && position < children.Length then
            children.[position]
        else
            Unchecked.defaultof<ISyntaxNode>

    override this.Emit (code:CodeBuilder) : unit =
        ()

[<AbstractClass>]
type TokenSyntax (lexeme:SyntaxLexeme) = 
    inherit SyntaxQueryNode ()

    member this.Lexeme with get () : SyntaxLexeme = lexeme

    override this.CountChildren () : int = 
        0

    override this.FindChildByPosition (position:int) : ISyntaxNode = 
        Unchecked.defaultof<ISyntaxNode>

    override this.Emit (code:CodeBuilder) : unit = 
        ()

[<Sealed>]
type IdentifierTokenSyntax (lexeme:SyntaxLexeme) =
    inherit TokenSyntax (lexeme)

[<Sealed>]
type OperatorTokenSyntax (lexeme:SyntaxLexeme) =
    inherit TokenSyntax (lexeme)

[<Sealed>]
type KeywordTokenSyntax (lexeme:SyntaxLexeme) =
    inherit TokenSyntax (lexeme)

[<Sealed>]
type AttributesSyntax (attributes:IdentifierTokenSyntax[]) =
    inherit SyntaxQueryNode ()

    member this.Contains (name:string) : bool =
        this.FindChildByPredicate (Func<ISyntaxNode, bool> (fun (node:ISyntaxNode) -> (node :? IdentifierTokenSyntax) && (node :?> IdentifierTokenSyntax).Lexeme = name)) <> null

    override this.CountChildren () : int =
        attributes.Length

    override this.FindChildByPosition (position:int) : ISyntaxNode =
        if position >= 0 && position < attributes.Length then
            attributes.[position] :> ISyntaxNode
        else
            Unchecked.defaultof<ISyntaxNode>

    override this.Emit (code:CodeBuilder) : unit =
        ()

[<AbstractClass>]
type StatementSyntax (attributes:AttributesSyntax) =
    inherit SyntaxQueryNode ()
    
    member this.Attributes with get () : AttributesSyntax = attributes

    override this.CountChildren () : int =
        1

    override this.FindChildByPosition (position:int) : ISyntaxNode =
        if position = 0 then
            attributes :> ISyntaxNode
        else
            Unchecked.defaultof<ISyntaxNode>

[<Sealed>]
type CollectStatementSyntax (attributes:AttributesSyntax, target:IdentifierTokenSyntax) =
    inherit StatementSyntax (attributes)

    member this.Target with get () : IdentifierTokenSyntax = target

    override this.CountChildren () : int =
        base.CountChildren () + 1

    override this.FindChildByPosition (position:int) : ISyntaxNode =
        let baseChildCount:int = base.CountChildren ()
        
        if position < baseChildCount then
            base.FindChildByPosition position
        else
            match position - baseChildCount with
            | 0 -> target :> ISyntaxNode
            | _ -> Unchecked.defaultof<ISyntaxNode>

    override this.Emit (code:CodeBuilder) : unit =
        ()

[<Sealed>]
type FilterStatementSyntax (attributes:AttributesSyntax, local:IdentifierTokenSyntax, remote:IdentifierTokenSyntax) =
    inherit StatementSyntax (attributes)

    member this.Local with get () : IdentifierTokenSyntax = local
    member this.Remote with get () : IdentifierTokenSyntax = remote

    override this.CountChildren () : int =
        base.CountChildren () + 2

    override this.FindChildByPosition (position:int) : ISyntaxNode =
        let baseChildCount:int = base.CountChildren ()
        
        if position < baseChildCount then
            base.FindChildByPosition position
        else
            match position - baseChildCount with
            | 0 -> local :> ISyntaxNode
            | 1 -> remote :> ISyntaxNode
            | _ -> Unchecked.defaultof<ISyntaxNode>

    override this.Emit (code:CodeBuilder) : unit =
        ()

[<Sealed>]
type CompilationUnitSyntax (children:ISyntaxNode[]) =
    inherit SyntaxQueryNode ()

    override this.CountChildren () : int = 
        children.Length

    override this.FindChildByPosition (position:int) : ISyntaxNode = 
        if position >= 0 && position < children.Length then
            children.[position]
        else
            Unchecked.defaultof<ISyntaxNode>

    override this.Emit (code:CodeBuilder) : unit =
        ()

namespace Gotcha.Core

open Gotcha.Core.Query

type SyntaxQueryLexer () as this =
    inherit SyntaxLexer ()

    do
        this.AddLexemeSeparators ([| ' '; '\r'; '\t'; |])

        this.AddSplitterLexemes ([| "\n"; "["; "]"; |])

        this.AddGrouperLexemeRanges ([| SyntaxLexemeRange ("[", "]", false); SyntaxLexemeRange (null, "\n", true); |])

        this.AddReservedLexeme ("[", SyntaxTokenKind.Operator)
        this.AddReservedLexeme ("]", SyntaxTokenKind.Operator)
        this.AddReservedLexeme ("\n", SyntaxTokenKind.Keyword)

        this.AddReservedLexeme ("COLLECT", SyntaxTokenKind.Keyword)
        this.AddReservedLexeme ("FILTER", SyntaxTokenKind.Keyword)

type SyntaxQueryParser () =
    inherit SyntaxParser ()

    override this.TransformToken (token:SyntaxToken) : ISyntaxNode =
        match token.Kind with
        | SyntaxTokenKind.IdentifierOrLiteral -> 
            (IdentifierTokenSyntax token.Lexeme) :> ISyntaxNode
        | SyntaxTokenKind.Operator -> 
            (OperatorTokenSyntax token.Lexeme) :> ISyntaxNode
        | SyntaxTokenKind.Keyword -> 
            (KeywordTokenSyntax token.Lexeme) :> ISyntaxNode
        | _ -> 
            Unchecked.defaultof<ISyntaxNode>

    override this.CombineNodes (nodes:ISyntaxNode[]) : ISyntaxNode =
        let nodeSequence:SyntaxPatternMatcher = SyntaxPatternMatcher nodes

        match nodeSequence.MatchFront<TokenSyntax> () with
        | Some (beginToken) ->
            match beginToken with
            | :? OperatorTokenSyntax as beginOperatorToken ->
                match beginOperatorToken.Lexeme with
                | "[" when nodeSequence.MatchBack(fun (node:OperatorTokenSyntax) -> node.Lexeme = "]").IsSome ->
                    match nodeSequence.MatchFrontRange<IdentifierTokenSyntax> (0, System.Int32.MaxValue) with
                    | Some (propertyIdentifiers) ->
                        if nodeSequence.RemainingNodes = 0 then
                            (AttributesSyntax propertyIdentifiers) :> ISyntaxNode
                        else
                            (UnknownSyntax nodes) :> ISyntaxNode
                    | None ->
                        (UnknownSyntax nodes) :> ISyntaxNode
                | _ ->
                    (UnknownSyntax nodes) :> ISyntaxNode
            | :? KeywordTokenSyntax as beginKeywordToken -> 
                match beginKeywordToken.Lexeme with
                | "COLLECT" ->
                    let attributes:AttributesSyntax = 
                        match nodeSequence.MatchFront<AttributesSyntax> () with
                        | Some (attributes) ->
                            attributes
                        | None ->
                            AttributesSyntax ([| |])

                    match nodeSequence.MatchFront<IdentifierTokenSyntax> () with
                    | Some (target) ->
                        if nodeSequence.RemainingNodes = 0 then
                            (CollectStatementSyntax (attributes, target)) :> ISyntaxNode
                        else
                            (UnknownSyntax nodes) :> ISyntaxNode
                    | None ->
                        (UnknownSyntax nodes) :> ISyntaxNode
                | "FILTER" ->
                    let attributes:AttributesSyntax = 
                        match nodeSequence.MatchFront<AttributesSyntax> () with
                        | Some (attributes) ->
                            attributes
                        | None ->
                            AttributesSyntax ([| |])

                    match nodeSequence.MatchFront<IdentifierTokenSyntax> () with
                    | Some (local) ->
                        match nodeSequence.MatchFront<IdentifierTokenSyntax> () with
                        | Some (remote) ->
                            if nodeSequence.RemainingNodes = 0 then
                                (FilterStatementSyntax (attributes, local, remote)) :> ISyntaxNode
                            else
                                (UnknownSyntax nodes) :> ISyntaxNode
                        | None ->
                            (UnknownSyntax nodes) :> ISyntaxNode
                    | None ->
                        (UnknownSyntax nodes) :> ISyntaxNode
                | _ -> 
                    (UnknownSyntax nodes) :> ISyntaxNode
            | _ -> 
                (UnknownSyntax nodes) :> ISyntaxNode
        | None ->
            (UnknownSyntax nodes) :> ISyntaxNode

    override this.FinaliseTree (nodes:ISyntaxNode[]) : ISyntaxNode =
        (CompilationUnitSyntax nodes) :> ISyntaxNode