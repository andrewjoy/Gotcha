﻿using Gotcha.Core;

namespace Gotcha.Common.Syntax
{
    abstract public class ModifierSyntaxNode : SyntaxNode
    {

    }

    sealed public class ModifierSyntaxNode<MODIFIER_TYPE> : ModifierSyntaxNode
        where MODIFIER_TYPE : struct
    {
        public MODIFIER_TYPE Value { get; } = default(MODIFIER_TYPE);

        public ModifierSyntaxNode(MODIFIER_TYPE value)
        {
            this.Value = value;
        }

        public override int CountChildren()
        {
            return 0;
        }

        public override ISyntaxNode FindChildByPosition(int position)
        {
            return null;
        }

        public override void Emit(CodeBuilder code)
        {
            code.Append(this.Value.ToString());
        }
    }
}
