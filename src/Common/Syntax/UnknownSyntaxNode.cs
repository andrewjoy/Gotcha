﻿using Gotcha.Core;

namespace Gotcha.Common.Syntax
{
    sealed public class UnknownSyntaxNode : SyntaxNode
    {
        private readonly ISyntaxNode[] children = null;

        public UnknownSyntaxNode(ISyntaxNode[] children)
        {
            this.children = children;
        }

        public override int CountChildren()
        {
            return children.Length;
        }

        public override ISyntaxNode FindChildByPosition(int position)
        {
            if (position >= 0 && position < children.Length)
            {
                return this.children[position];
            }

            return null;
        }

        public override void Emit(CodeBuilder code)
        {
            code.AppendLine();

            code.AppendLine("begin unknown");

            code.AddIndent();

            for (int i = 0; i < this.children.Length; ++i)
            {
                this.children[i].Emit(code);

                code.AppendLine();
            }

            code.RemoveIndent();

            code.AppendLine("end unknown");
        }
    }
}
