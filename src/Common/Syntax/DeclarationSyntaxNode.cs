﻿using Gotcha.Core;

namespace Gotcha.Common.Syntax
{
    abstract public class DeclarationSyntaxNode : SyntaxNode
    {
        public enum ChildPosition
        {
            Modifiers = 0,
            Identifier = 1
        }

        public ModifierListSyntaxNode Modifiers { get; } = null;

        public IdentifierTokenSyntaxNode Identifier { get; } = null;

        public DeclarationSyntaxNode(ModifierListSyntaxNode modifiers, IdentifierTokenSyntaxNode identifier)
        {
            this.Modifiers = modifiers;
            this.Identifier = identifier;
        }

        public override int CountChildren()
        {
            return 2;
        }

        public override ISyntaxNode FindChildByPosition(int position)
        {
            switch (position)
            {
                case 0:
                    return this.Modifiers;
                case 1:
                    return this.Identifier;
            }

            return null;
        }

        public ISyntaxNode FindChildByPosition(ChildPosition position)
        {
            return FindChildByPosition((int)position);
        }

        public override void Emit(CodeBuilder code)
        {
            code.Append("declare ");

            this.Identifier.Emit(code);

            if (this.Modifiers.CountChildren() > 0)
            {
                code.Append(" as ");

                this.Modifiers.Emit(code);
            }
        }
    }
}
