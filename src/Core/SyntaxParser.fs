﻿namespace Gotcha.Core

/// <summary>
/// General purpose parser with derivable pipeline stages.
/// </summary>
[<AbstractClass>]
type SyntaxParser () =
    /// <summary>
    /// Transforms a <paramref name="token"/> into a syntax node. The type of the node returned depends on the implementation.
    /// </summary>
    /// <param name="token">Token to process.</param>
    /// <returns>Processed syntax node.</returns>
    abstract member TransformToken : token:SyntaxToken -> ISyntaxNode

    /// <summary>
    /// Combines a sequence of syntax <paramref name="nodes"/> into a single parent node. The type of node returned depends on the implementation.
    /// </summary>
    /// <param name="nodes">Node sequence to process.</param>
    /// <returns>Processed syntax node.</returns>
    abstract member CombineNodes : nodes:ISyntaxNode[] -> ISyntaxNode

    /// <summary>
    /// Processes a sequence of syntax <paramref name="nodes"/> into a single root node, usually a 'compilation unit'.
    /// This is the final stage of parsing, so the entire tree of nodes are present and can be manipulated.
    /// </summary>
    /// <param name="nodes">Node sequence to process.</param>
    /// <returns>Processed root syntax node.</returns>
    abstract member FinaliseTree : nodes:ISyntaxNode[] -> ISyntaxNode
    
    /// <summary>
    /// Constructs a tree of syntax nodes out of a <paramref name="tokenHeirarchy"/>.
    /// </summary>
    /// <param name="tokenHeirarchy">Heirarchy of tokens to transform.</param>
    /// <returns>Nodal representation of input tokens.</returns>
    abstract member ProcessTokens : tokenHeirarchy:SyntaxTokenHeirarchy -> ISyntaxNode
    override this.ProcessTokens (tokenHeirarchy:SyntaxTokenHeirarchy) : ISyntaxNode =
        // Recursively (depth-first) constructs syntax node tree
        let rec BuildNode (heirarchy:SyntaxTokenHeirarchy, isRoot:bool) : ISyntaxNode =
            match heirarchy with
            | Leaf (token:SyntaxToken) -> 
                this.TransformToken token
            | Parent (childHeirarchies:SyntaxTokenHeirarchy[]) -> 
                let childNodes:ISyntaxNode[] = Array.zeroCreate<ISyntaxNode> childHeirarchies.Length

                for childIndex = 0 to childHeirarchies.Length - 1 do
                    childNodes.[childIndex] <- BuildNode (childHeirarchies.[childIndex], false)

                if isRoot then
                    this.FinaliseTree childNodes
                else
                    this.CombineNodes childNodes

        BuildNode (tokenHeirarchy, true)