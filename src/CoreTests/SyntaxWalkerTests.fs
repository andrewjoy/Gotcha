﻿namespace Gotcha.CoreTests

open System
open System.Collections.Generic

open Gotcha.Core

open NUnit.Framework

[<TestFixture>]
type SyntaxWalkerTests () =
    [<Test>]
    static member FindPositionOf_ReturnsCorrectNodePosition () : unit =
        let targetNode:ISyntaxNode = DummySyntaxNode () :> ISyntaxNode

        let rootNode:ISyntaxNode = DummySyntaxNode [| DummySyntaxNode (); DummySyntaxNode (); targetNode |] :> ISyntaxNode

        Assert.AreEqual (2, SyntaxWalker.FindPositionOf (targetNode, rootNode))

    [<Test>]
    static member FindPositionOf_ReturnsCorrectScaffoldPosition () : unit = 
        let targetNode:ISyntaxNode = DummySyntaxNode () :> ISyntaxNode

        let rootScaffold:SyntaxScaffold = SyntaxScaffold (DummySyntaxNode [| DummySyntaxNode (); DummySyntaxNode (); targetNode |])

        Assert.AreEqual (2, SyntaxWalker.FindPositionOf (rootScaffold.GetChildAt 2))
    
    [<Test>]
    static member VisitDown_TouchesAllNodes () : unit =
        let targetNodes:List<ISyntaxNode> = List<ISyntaxNode> ()

        let AddTargetNode (node:ISyntaxNode) : ISyntaxNode =
            targetNodes.Add node

            node

        let RemoveTargetNode (node:ISyntaxNode) : unit =
            ignore (targetNodes.Remove node)

        let rootNode:ISyntaxNode = AddTargetNode (DummySyntaxNode [| AddTargetNode (DummySyntaxNode ()); AddTargetNode (DummySyntaxNode ()) |])

        SyntaxWalker.VisitDown (rootNode, RemoveTargetNode)

        Assert.IsEmpty targetNodes

    [<Test>]
    static member VisitDown_TouchesAllScaffolds () : unit =
        let targetNodes:List<ISyntaxNode> = List<ISyntaxNode> ()

        let AddTargetNode (node:ISyntaxNode) : ISyntaxNode =
            targetNodes.Add node

            node

        let RemoveTargetScaffoldNode (scaffold:SyntaxScaffold) : unit =
            ignore (targetNodes.Remove scaffold.Node)

        let rootNode:ISyntaxNode = AddTargetNode (DummySyntaxNode [| AddTargetNode (DummySyntaxNode ()); AddTargetNode (DummySyntaxNode ()) |])

        let rootScaffold:SyntaxScaffold = SyntaxScaffold rootNode

        SyntaxWalker.VisitDown (rootScaffold, RemoveTargetScaffoldNode)

        Assert.IsEmpty targetNodes
    
    [<Test>]
    static member VisitDownWithType_TouchesAllNodes () : unit =
        let targetNodes:List<ISyntaxNode> = List<ISyntaxNode> ()

        let AddTargetNode (node:ISyntaxNode) : ISyntaxNode =
            targetNodes.Add node

            node

        let RemoveTargetNode (node:ISyntaxNode) : unit =
            ignore (targetNodes.Remove node)

        let rootNode:ISyntaxNode = AddTargetNode (DummySyntaxNode [| AddTargetNode (DummySyntaxNode ()); AddTargetNode (DummySyntaxNode ()) |])

        SyntaxWalker.VisitDownWithType (typeof<DummySyntaxNode>, rootNode, RemoveTargetNode)

        Assert.IsEmpty targetNodes

    [<Test>]
    static member VisitDownWithTypeGeneric_TouchesAllNodes () : unit =
        let targetNodes:List<ISyntaxNode> = List<ISyntaxNode> ()

        let AddTargetNode (node:ISyntaxNode) : ISyntaxNode =
            targetNodes.Add node

            node

        let RemoveTargetNode (node:DummySyntaxNode) : unit =
            ignore (targetNodes.Remove node)

        let rootNode:ISyntaxNode = AddTargetNode (DummySyntaxNode [| AddTargetNode (DummySyntaxNode ()); AddTargetNode (DummySyntaxNode ()) |])

        SyntaxWalker.VisitDownWithType<DummySyntaxNode> (rootNode, RemoveTargetNode)

        Assert.IsEmpty targetNodes

    [<Test>]
    static member VisitDownWithType_TouchesAllScaffolds () : unit =
        let targetNodes:List<ISyntaxNode> = List<ISyntaxNode> ()

        let AddTargetNode (node:ISyntaxNode) : ISyntaxNode =
            targetNodes.Add node

            node

        let RemoveTargetScaffoldNode (scaffold:SyntaxScaffold) : unit =
            ignore (targetNodes.Remove scaffold.Node)

        let rootNode:ISyntaxNode = AddTargetNode (DummySyntaxNode [| AddTargetNode (DummySyntaxNode ()); AddTargetNode (DummySyntaxNode ()) |])

        let rootScaffold:SyntaxScaffold = SyntaxScaffold rootNode

        SyntaxWalker.VisitDownWithType (typeof<DummySyntaxNode>, rootScaffold, RemoveTargetScaffoldNode)

        Assert.IsEmpty targetNodes

    [<Test>]
    static member VisitDownWithTypeGeneric_TouchesAllScaffolds () : unit =
        let targetNodes:List<ISyntaxNode> = List<ISyntaxNode> ()

        let AddTargetNode (node:ISyntaxNode) : ISyntaxNode =
            targetNodes.Add node

            node

        let RemoveTargetScaffoldNode (scaffold:SyntaxScaffold) : unit =
            ignore (targetNodes.Remove scaffold.Node)

        let rootNode:ISyntaxNode = AddTargetNode (DummySyntaxNode [| AddTargetNode (DummySyntaxNode ()); AddTargetNode (DummySyntaxNode ()) |])

        let rootScaffold:SyntaxScaffold = SyntaxScaffold rootNode

        SyntaxWalker.VisitDownWithType<DummySyntaxNode> (rootScaffold, RemoveTargetScaffoldNode)

        Assert.IsEmpty targetNodes

    [<Test>]
    static member VisitUp_TouchesAllScaffolds () : unit =
        let targetNodes:List<ISyntaxNode> = List<ISyntaxNode> ()

        let AddTargetNode (node:ISyntaxNode) : ISyntaxNode =
            targetNodes.Add node

            node

        let RemoveTargetScaffoldNode (scaffold:SyntaxScaffold) : unit =
            ignore (targetNodes.Remove scaffold.Node)

        let rootNode:ISyntaxNode = AddTargetNode (DummySyntaxNode [| AddTargetNode (DummySyntaxNode ()); AddTargetNode (DummySyntaxNode ()) |])

        let rootScaffold:SyntaxScaffold = SyntaxScaffold rootNode

        SyntaxWalker.VisitUp (rootScaffold.GetChildAt 0, Action<SyntaxScaffold> RemoveTargetScaffoldNode)

        Assert.AreEqual (1, targetNodes.Count)

    [<Test>]
    static member VisitUpWithType_TouchesAllScaffolds () : unit =
        let targetNodes:List<ISyntaxNode> = List<ISyntaxNode> ()

        let AddTargetNode (node:ISyntaxNode) : ISyntaxNode =
            targetNodes.Add node

            node

        let RemoveTargetScaffoldNode (scaffold:SyntaxScaffold) : unit =
            ignore (targetNodes.Remove scaffold.Node)

        let rootNode:ISyntaxNode = AddTargetNode (DummySyntaxNode [| AddTargetNode (DummySyntaxNode ()); AddTargetNode (DummySyntaxNode ()) |])

        let rootScaffold:SyntaxScaffold = SyntaxScaffold rootNode

        SyntaxWalker.VisitUpWithType (typeof<DummySyntaxNode>, rootScaffold.GetChildAt 0, Action<SyntaxScaffold> RemoveTargetScaffoldNode)

        Assert.AreEqual (1, targetNodes.Count)

    [<Test>]
    static member VisitUpWithTypeGeneric_TouchesAllScaffolds () : unit =
        let targetNodes:List<ISyntaxNode> = List<ISyntaxNode> ()

        let AddTargetNode (node:ISyntaxNode) : ISyntaxNode =
            targetNodes.Add node

            node

        let RemoveTargetScaffoldNode (scaffold:SyntaxScaffold) : unit =
            ignore (targetNodes.Remove scaffold.Node)

        let rootNode:ISyntaxNode = AddTargetNode (DummySyntaxNode [| AddTargetNode (DummySyntaxNode ()); AddTargetNode (DummySyntaxNode ()) |])

        let rootScaffold:SyntaxScaffold = SyntaxScaffold rootNode

        SyntaxWalker.VisitUpWithType<DummySyntaxNode> (rootScaffold.GetChildAt 0, Action<SyntaxScaffold> RemoveTargetScaffoldNode)

        Assert.AreEqual (1, targetNodes.Count)

    [<Test>]
    static member VisitLeft_TouchesAllScaffolds () : unit =
        let targetNodes:List<ISyntaxNode> = List<ISyntaxNode> ()

        let AddTargetNode (node:ISyntaxNode) : ISyntaxNode =
            targetNodes.Add node

            node

        let RemoveTargetScaffoldNode (scaffold:SyntaxScaffold) : unit =
            ignore (targetNodes.Remove scaffold.Node)

        let rootNode:ISyntaxNode = AddTargetNode (DummySyntaxNode [| AddTargetNode (DummySyntaxNode ()); AddTargetNode (DummySyntaxNode ()); AddTargetNode (DummySyntaxNode ()); AddTargetNode (DummySyntaxNode ()) |])

        let rootScaffold:SyntaxScaffold = SyntaxScaffold rootNode

        SyntaxWalker.VisitLeft (rootScaffold.GetChildAt 2, Action<SyntaxScaffold> RemoveTargetScaffoldNode)

        Assert.AreEqual (2, targetNodes.Count)

    [<Test>]
    static member VisitLeftWithType_TouchesAllScaffolds () : unit =
        let targetNodes:List<ISyntaxNode> = List<ISyntaxNode> ()

        let AddTargetNode (node:ISyntaxNode) : ISyntaxNode =
            targetNodes.Add node

            node

        let RemoveTargetScaffoldNode (scaffold:SyntaxScaffold) : unit =
            ignore (targetNodes.Remove scaffold.Node)

        let rootNode:ISyntaxNode = AddTargetNode (DummySyntaxNode [| AddTargetNode (DummySyntaxNode ()); AddTargetNode (DummySyntaxNode ()); AddTargetNode (DummySyntaxNode ()); AddTargetNode (DummySyntaxNode ()) |])

        let rootScaffold:SyntaxScaffold = SyntaxScaffold rootNode

        SyntaxWalker.VisitLeftWithType (typeof<DummySyntaxNode>, rootScaffold.GetChildAt 2, Action<SyntaxScaffold> RemoveTargetScaffoldNode)

        Assert.AreEqual (2, targetNodes.Count)

    [<Test>]
    static member VisitLeftWithTypeGeneric_TouchesAllScaffolds () : unit =
        let targetNodes:List<ISyntaxNode> = List<ISyntaxNode> ()

        let AddTargetNode (node:ISyntaxNode) : ISyntaxNode =
            targetNodes.Add node

            node

        let RemoveTargetScaffoldNode (scaffold:SyntaxScaffold) : unit =
            ignore (targetNodes.Remove scaffold.Node)

        let rootNode:ISyntaxNode = AddTargetNode (DummySyntaxNode [| AddTargetNode (DummySyntaxNode ()); AddTargetNode (DummySyntaxNode ()); AddTargetNode (DummySyntaxNode ()); AddTargetNode (DummySyntaxNode ()) |])

        let rootScaffold:SyntaxScaffold = SyntaxScaffold rootNode

        SyntaxWalker.VisitLeftWithType<DummySyntaxNode> (rootScaffold.GetChildAt 2, Action<SyntaxScaffold> RemoveTargetScaffoldNode)

        Assert.AreEqual (2, targetNodes.Count)

    [<Test>]
    static member VisitRight_TouchesAllScaffolds () : unit =
        let targetNodes:List<ISyntaxNode> = List<ISyntaxNode> ()

        let AddTargetNode (node:ISyntaxNode) : ISyntaxNode =
            targetNodes.Add node

            node

        let RemoveTargetScaffoldNode (scaffold:SyntaxScaffold) : unit =
            ignore (targetNodes.Remove scaffold.Node)

        let rootNode:ISyntaxNode = AddTargetNode (DummySyntaxNode [| AddTargetNode (DummySyntaxNode ()); AddTargetNode (DummySyntaxNode ()); AddTargetNode (DummySyntaxNode ()); AddTargetNode (DummySyntaxNode ()) |])

        let rootScaffold:SyntaxScaffold = SyntaxScaffold rootNode

        SyntaxWalker.VisitRight (rootScaffold.GetChildAt 1, Action<SyntaxScaffold> RemoveTargetScaffoldNode)

        Assert.AreEqual (2, targetNodes.Count)
    
    [<Test>]
    static member VisitRightWithType_TouchesAllScaffolds () : unit =
        let targetNodes:List<ISyntaxNode> = List<ISyntaxNode> ()

        let AddTargetNode (node:ISyntaxNode) : ISyntaxNode =
            targetNodes.Add node

            node

        let RemoveTargetScaffoldNode (scaffold:SyntaxScaffold) : unit =
            ignore (targetNodes.Remove scaffold.Node)

        let rootNode:ISyntaxNode = AddTargetNode (DummySyntaxNode [| AddTargetNode (DummySyntaxNode ()); AddTargetNode (DummySyntaxNode ()); AddTargetNode (DummySyntaxNode ()); AddTargetNode (DummySyntaxNode ()) |])

        let rootScaffold:SyntaxScaffold = SyntaxScaffold rootNode

        SyntaxWalker.VisitRightWithType (typeof<DummySyntaxNode>, rootScaffold.GetChildAt 1, Action<SyntaxScaffold> RemoveTargetScaffoldNode)

        Assert.AreEqual (2, targetNodes.Count)

    [<Test>]
    static member VisitRightWithTypeGeneric_TouchesAllScaffolds () : unit =
        let targetNodes:List<ISyntaxNode> = List<ISyntaxNode> ()

        let AddTargetNode (node:ISyntaxNode) : ISyntaxNode =
            targetNodes.Add node

            node

        let RemoveTargetScaffoldNode (scaffold:SyntaxScaffold) : unit =
            ignore (targetNodes.Remove scaffold.Node)

        let rootNode:ISyntaxNode = AddTargetNode (DummySyntaxNode [| AddTargetNode (DummySyntaxNode ()); AddTargetNode (DummySyntaxNode ()); AddTargetNode (DummySyntaxNode ()); AddTargetNode (DummySyntaxNode ()) |])

        let rootScaffold:SyntaxScaffold = SyntaxScaffold rootNode

        SyntaxWalker.VisitRightWithType<DummySyntaxNode> (rootScaffold.GetChildAt 1, Action<SyntaxScaffold> RemoveTargetScaffoldNode)

        Assert.AreEqual (2, targetNodes.Count)

    [<Test>]
    static member VisitLeftAndRight_TouchesAllScaffolds () : unit =
        let targetNodes:List<ISyntaxNode> = List<ISyntaxNode> ()

        let AddTargetNode (node:ISyntaxNode) : ISyntaxNode =
            targetNodes.Add node

            node

        let RemoveTargetScaffoldNode (scaffold:SyntaxScaffold) : unit =
            ignore (targetNodes.Remove scaffold.Node)

        let rootNode:ISyntaxNode = AddTargetNode (DummySyntaxNode [| AddTargetNode (DummySyntaxNode ()); AddTargetNode (DummySyntaxNode ()); AddTargetNode (DummySyntaxNode ()); AddTargetNode (DummySyntaxNode ()) |])

        let rootScaffold:SyntaxScaffold = SyntaxScaffold rootNode

        SyntaxWalker.VisitLeftAndRight (rootScaffold.GetChildAt 1, Action<SyntaxScaffold> RemoveTargetScaffoldNode)

        Assert.AreEqual (1, targetNodes.Count)

    [<Test>]
    static member VisitLeftAndRightWithType_TouchesAllScaffolds () : unit =
        let targetNodes:List<ISyntaxNode> = List<ISyntaxNode> ()

        let AddTargetNode (node:ISyntaxNode) : ISyntaxNode =
            targetNodes.Add node

            node

        let RemoveTargetScaffoldNode (scaffold:SyntaxScaffold) : unit =
            ignore (targetNodes.Remove scaffold.Node)

        let rootNode:ISyntaxNode = AddTargetNode (DummySyntaxNode [| AddTargetNode (DummySyntaxNode ()); AddTargetNode (DummySyntaxNode ()); AddTargetNode (DummySyntaxNode ()); AddTargetNode (DummySyntaxNode ()) |])

        let rootScaffold:SyntaxScaffold = SyntaxScaffold rootNode

        SyntaxWalker.VisitLeftAndRightWithType (typeof<DummySyntaxNode>, rootScaffold.GetChildAt 1, Action<SyntaxScaffold> RemoveTargetScaffoldNode)

        Assert.AreEqual (1, targetNodes.Count)

    [<Test>]
    static member VisitLeftAndRightWithTypeGeneric_TouchesAllScaffolds () : unit =
        let targetNodes:List<ISyntaxNode> = List<ISyntaxNode> ()

        let AddTargetNode (node:ISyntaxNode) : ISyntaxNode =
            targetNodes.Add node

            node

        let RemoveTargetScaffoldNode (scaffold:SyntaxScaffold) : unit =
            ignore (targetNodes.Remove scaffold.Node)

        let rootNode:ISyntaxNode = AddTargetNode (DummySyntaxNode [| AddTargetNode (DummySyntaxNode ()); AddTargetNode (DummySyntaxNode ()); AddTargetNode (DummySyntaxNode ()); AddTargetNode (DummySyntaxNode ()) |])

        let rootScaffold:SyntaxScaffold = SyntaxScaffold rootNode

        SyntaxWalker.VisitLeftAndRightWithType<DummySyntaxNode> (rootScaffold.GetChildAt 1, Action<SyntaxScaffold> RemoveTargetScaffoldNode)

        Assert.AreEqual (1, targetNodes.Count)