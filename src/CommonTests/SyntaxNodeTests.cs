﻿using System;

using Gotcha.Core;
using Gotcha.Common.Syntax;

using NUnit.Framework;

namespace Gotcha.CommonTests
{
    [TestFixture]
    static public class SyntaxNodeTests
    {
        sealed private class DummySyntaxNode : SyntaxNode
        {
            private readonly ISyntaxNode[] children = null;

            public DummySyntaxNode()
            {
                this.children = new ISyntaxNode[0];
            }

            public DummySyntaxNode(ISyntaxNode[] children)
            {
                this.children = children;
            }

            public override int CountChildren()
            {
                return children.Length;
            }

            public override ISyntaxNode FindChildByPosition(int position)
            {
                return children[position];
            }

            public override void Emit(CodeBuilder code)
            {
                
            }
        }

        #region BASE_TESTS
        [Test]
        static public void Base_FindChildByType_FindsAtAllPositions()
        {
            ISyntaxNode[] children = new ISyntaxNode[] { new DummySyntaxNode(), new DummySyntaxNode(), new DummySyntaxNode() };
            Internal_FindChildByType_FindsAtAllPositions(new DummySyntaxNode(children));
        }

        [Test]
        static public void Base_FindChildByTypeGeneric_FindsAtAllPositions()
        {
            ISyntaxNode[] children = new ISyntaxNode[] { new DummySyntaxNode(), new DummySyntaxNode(), new DummySyntaxNode() };
            Internal_FindChildByTypeGeneric_FindsAtAllPositions(new DummySyntaxNode(children));
        }

        [Test]
        static public void Base_FindChildByPredicate_FindsAtAllPositions()
        {
            ISyntaxNode[] children = new ISyntaxNode[] { new DummySyntaxNode(new ISyntaxNode[2]), new DummySyntaxNode(new ISyntaxNode[2]), new DummySyntaxNode(new ISyntaxNode[2]) };
            Internal_FindChildByPredicate_FindsAtAllPositions(new DummySyntaxNode(children), (ISyntaxNode node) => node.CountChildren() == 2);
        }

        [Test]
        static public void Base_FindChildByPredicate_IsAccurate()
        {
            ISyntaxNode[] children = new ISyntaxNode[] { new DummySyntaxNode(), new DummySyntaxNode(new ISyntaxNode[2]), new DummySyntaxNode() };

            Func<ISyntaxNode, bool> predicate = (ISyntaxNode node) => node.CountChildren() == 2;

            Assert.True(predicate.Invoke(new DummySyntaxNode(children).FindChildByPredicate(predicate)));
        }

        [Test]
        static public void Base_FindChildrenByType_FindsAtAllPositions()
        {
            ISyntaxNode[] children = new ISyntaxNode[] { new DummySyntaxNode(), new DummySyntaxNode(), new DummySyntaxNode() };
            Internal_FindChildrenByType_FindsAtAllPositions(new DummySyntaxNode(children), children[0].GetType());
        }

        [Test]
        static public void Base_FindChildrenByTypeGeneric_FindsAtAllPositions()
        {
            ISyntaxNode[] children = new ISyntaxNode[] { new DummySyntaxNode(), new DummySyntaxNode(), new DummySyntaxNode() };
            Internal_FindChildrenByTypeGeneric_FindsAtAllPositions(new DummySyntaxNode(children));
        }

        [Test]
        static public void Base_FindChildrenByPredicate_FindsAtAllPositions()
        {
            ISyntaxNode[] children = new ISyntaxNode[] { new DummySyntaxNode(new ISyntaxNode[2]), new DummySyntaxNode(new ISyntaxNode[2]), new DummySyntaxNode(new ISyntaxNode[2]) };
            Internal_FindChildrenByPredicate_FindsAtAllPositions(new DummySyntaxNode(children), (ISyntaxNode node) => node.CountChildren() == 2);
        }

        [Test]
        static public void Base_FindChildrenByPredicate_IsAccurate()
        {
            ISyntaxNode[] children = new ISyntaxNode[] { new DummySyntaxNode(), new DummySyntaxNode(new ISyntaxNode[2]), new DummySyntaxNode(), new DummySyntaxNode(new ISyntaxNode[2]), new DummySyntaxNode() };

            Func<ISyntaxNode, bool> predicate = (ISyntaxNode node) => node.CountChildren() == 2;

            ISyntaxNode[] foundChildren = new DummySyntaxNode(children).FindChildrenByPredicate(predicate);

            Assert.AreEqual(2, foundChildren.Length);

            for (int i = 0; i < foundChildren.Length; ++i)
            {
                Assert.True(predicate.Invoke(foundChildren[i]));
            }
        }
        #endregion

        #region ARGUMENTLIST_TESTS
        [Test]
        static public void ArgumentList_FindChildByPosition_ReturnsNoNulls()
        {
            ArgumentSyntaxNode[] arguments = new ArgumentSyntaxNode[] {
                new TokenArgumentSyntaxNode(new IdentifierTokenSyntaxNode("Console")),
                new TokenArgumentSyntaxNode(new IdentifierTokenSyntaxNode("WriteLine"))
            };

            Internal_FindChildByPosition_ReturnsNoNulls(new ArgumentListSyntaxNode(arguments));
        }
        #endregion

        #region COMPILATIONUNIT_TESTS
        [Test]
        static public void CompilationUnit_FindChildByPosition_ReturnsNoNulls()
        {
            ISyntaxNode[] children = new ISyntaxNode[] {
                new DummySyntaxNode(),
                new DummySyntaxNode(),
                new DummySyntaxNode()
            };

            Internal_FindChildByPosition_ReturnsNoNulls(new CompilationUnitSyntaxNode(children));
        }
        #endregion

        #region DECLARATIONSTATEMENT_TESTS
        sealed private class DummyDeclarationSyntaxNode : DeclarationSyntaxNode
        {
            public DummyDeclarationSyntaxNode(ModifierListSyntaxNode modifiers, IdentifierTokenSyntaxNode identifier) : base(modifiers, identifier) { }
        }

        [Test]
        static public void DeclarationStatement_FindChildByPosition_ReturnsNoNulls()
        {
            ModifierSyntaxNode[] modifiers = new ModifierSyntaxNode[] {

            };

            Internal_FindChildByPosition_ReturnsNoNulls(new DeclarationStatementSyntaxNode(new DummyDeclarationSyntaxNode(new ModifierListSyntaxNode(modifiers), new IdentifierTokenSyntaxNode("MyClass")), new ScopeSyntaxNode(new ISyntaxNode[] { })));
        }
        #endregion

        #region DECLARATION_TESTS
        [Test]
        static public void Declaration_FindChildByPosition_ReturnsNoNulls()
        {
            ModifierSyntaxNode[] modifiers = new ModifierSyntaxNode[] {

            };

            Internal_FindChildByPosition_ReturnsNoNulls(new DummyDeclarationSyntaxNode(new ModifierListSyntaxNode(modifiers), new IdentifierTokenSyntaxNode("MyClass")));
        }
        #endregion

        #region INVOCATIONEXPRESSION_TESTS
        [Test]
        static public void InvocationExpression_FindChildByPosition_ReturnsNoNulls()
        {
            IdentifierTokenSyntaxNode[] methodIdentifiers = new IdentifierTokenSyntaxNode[] {
                new IdentifierTokenSyntaxNode("Console"),
                new IdentifierTokenSyntaxNode("WriteLine")
            };

            ArgumentSyntaxNode[] arguments = new ArgumentSyntaxNode[] {
                new TokenArgumentSyntaxNode(new IdentifierTokenSyntaxNode("message"))
            };

            Internal_FindChildByPosition_ReturnsNoNulls(new InvocationExpressionSyntaxNode(new MemberAccessExpressionSyntaxNode(methodIdentifiers), new ArgumentListSyntaxNode(arguments)));
        }
        #endregion

        #region MEMBERACCESS_TESTS
        [Test]
        static public void MemberAccessExpression_FindChildByPosition_ReturnsNoNulls()
        {
            IdentifierTokenSyntaxNode[] identifiers = new IdentifierTokenSyntaxNode[] {
                new IdentifierTokenSyntaxNode("Console"),
                new IdentifierTokenSyntaxNode("WriteLine")
            };

            Internal_FindChildByPosition_ReturnsNoNulls(new MemberAccessExpressionSyntaxNode(identifiers));
        }
        #endregion

        #region METHODDECLARATION_TESTS
        [Test]
        static public void MethodDeclaration_FindChildByPosition_ReturnsNoNulls()
        {
            ModifierSyntaxNode[] modifiers = new ModifierSyntaxNode[] {

            };

            Internal_FindChildByPosition_ReturnsNoNulls(new MethodDeclarationSyntaxNode(new ModifierListSyntaxNode(modifiers), new IdentifierTokenSyntaxNode("MyClass"), new ArgumentListSyntaxNode(new ArgumentSyntaxNode[] { })));
        }
        #endregion

        #region MODIFIERLIST_TESTS
        sealed private class DummyModifierSyntaxNode : ModifierSyntaxNode
        {
            public override int CountChildren() { return 0; }

            public override ISyntaxNode FindChildByPosition(int position) { return null; }

            public override void Emit(CodeBuilder code) { }
        }

        [Test]
        static public void ModifierList_FindChildByPosition_ReturnsNoNulls()
        {
            ModifierSyntaxNode[] modifiers = new ModifierSyntaxNode[] {
                new DummyModifierSyntaxNode(),
                new DummyModifierSyntaxNode()
            };

            Internal_FindChildByPosition_ReturnsNoNulls(new ModifierListSyntaxNode(modifiers));
        }
        #endregion

        #region MODIFIER_TESTS
        public enum Modifier { Internal = 0 }

        [Test]
        static public void Modifier_FindChildByPosition_ReturnsNoNulls()
        {
            Internal_FindChildByPosition_ReturnsNoNulls(new ModifierSyntaxNode<Modifier>(Modifier.Internal));
        }
        #endregion

        #region SCOPE_TESTS
        [Test]
        static public void Scope_FindChildByPosition_ReturnsNoNulls()
        {
            ISyntaxNode[] children = new ISyntaxNode[] {
                new DummySyntaxNode(),
                new DummySyntaxNode(),
                new DummySyntaxNode()
            };

            Internal_FindChildByPosition_ReturnsNoNulls(new ScopeSyntaxNode(children));
        }
        #endregion

        #region TOKENARGUMENT_TESTS
        [Test]
        static public void TokenArgument_FindChildByPosition_ReturnsNoNulls()
        {
            Internal_FindChildByPosition_ReturnsNoNulls(new TokenArgumentSyntaxNode(new IdentifierTokenSyntaxNode("x")));
        }
        #endregion

        #region TOKEN_TESTS
        sealed private class DummyTokenSyntaxNode : TokenSyntaxNode
        {
            public DummyTokenSyntaxNode()
                : base("")
            {

            }

            public override int CountChildren() { return 0; }

            public override ISyntaxNode FindChildByPosition(int position) { return null; }

            public override void Emit(CodeBuilder code) { }
        }

        [Test]
        static public void Token_FindChildByPosition_ReturnsNoNulls()
        {
            Internal_FindChildByPosition_ReturnsNoNulls(new DummyTokenSyntaxNode());
        }
        #endregion

        #region UNKNOWN_TESTS
        [Test]
        static public void Unknown_FindChildByPosition_ReturnsNoNulls()
        {
            ISyntaxNode[] children = new ISyntaxNode[] {
                new DummySyntaxNode(),
                new DummySyntaxNode(),
                new DummySyntaxNode()
            };

            Internal_FindChildByPosition_ReturnsNoNulls(new UnknownSyntaxNode(children));
        }
        #endregion

        #region HELPERS
        /// <summary>
        /// Tests whether a syntax node returns useful values at each child position through ISyntaxNode::FindChildByPosition.
        /// </summary>
        /// <param name="testNode">The node to test.</param>
        static private void Internal_FindChildByPosition_ReturnsNoNulls(ISyntaxNode testNode)
        {
            int childCount = testNode.CountChildren();

            for (int i = 0; i < childCount; ++i)
            {
                Assert.NotNull(testNode.FindChildByPosition(i));
            }
        }

        /// <summary>
        /// Tests whether each position is accessible by ISyntaxNode::FindChildByType.
        /// </summary>
        /// <param name="testNode">The node to test.</param>
        static private void Internal_FindChildByType_FindsAtAllPositions(ISyntaxNode testNode)
        {
            int childCount = testNode.CountChildren();

            for (int i = 0; i < childCount; ++i)
            {
                ISyntaxNode childNode = testNode.FindChildByPosition(i);

                Assert.AreEqual(childNode, testNode.FindChildByType(childNode.GetType(), i));
            }
        }

        /// <summary>
        /// Tests whether each position is accessible by ISyntaxNode::FindChildByType. In order for this test to work, all children of <paramref name="testNode"/> must be of type <typeparamref name="NODE_TYPE"/>.
        /// </summary>
        /// <typeparam name="NODE_TYPE">Type of node to find.</param>
        /// <param name="testNode">The node to test.</param>
        static private void Internal_FindChildByTypeGeneric_FindsAtAllPositions<NODE_TYPE>(NODE_TYPE testNode)
            where NODE_TYPE : class, ISyntaxNode
        {
            int childCount = testNode.CountChildren();

            for (int i = 0; i < childCount; ++i)
            {
                Assert.AreEqual(testNode.FindChildByPosition(i), testNode.FindChildByType<NODE_TYPE>(i));
            }
        }

        /// <summary>
        /// Tests whether each position is accessible by ISyntaxNode::FindChildByPredicate. In order for this test to work, all children of <paramref name="testNode"/> must satisfy <paramref name="predicate"/>.
        /// </summary>
        /// <param name="testNode">The node to test.</param>
        /// <param name="predicate">The predicate to find by.</param>
        static private void Internal_FindChildByPredicate_FindsAtAllPositions(ISyntaxNode testNode, Func<ISyntaxNode, bool> predicate)
        {
            int childCount = testNode.CountChildren();

            for (int i = 0; i < childCount; ++i)
            {
                Assert.AreEqual(testNode.FindChildByPosition(i), testNode.FindChildByPredicate(predicate, i));
            }
        }

        /// <summary>
        /// Tests whether each position is accessible by ISyntaxNode::FindChildrenByType. In order for this test to work, all children of <paramref name="testNode"/> must be of type <paramref name="testNodeChildrenType"/>.
        /// </summary>
        /// <param name="testNode">The node to test.</param>
        /// <param name="testNodeChildrenType">The type of all children of <paramref name="testNode"/>.</param>
        static private void Internal_FindChildrenByType_FindsAtAllPositions(ISyntaxNode testNode, Type testNodeChildrenType)
        {
            int childCount = testNode.CountChildren();

            ISyntaxNode[] foundChildren = testNode.FindChildrenByType(testNodeChildrenType);

            Assert.AreEqual(childCount, foundChildren.Length);

            for (int i = 0; i < childCount; ++i)
            {
                Assert.AreEqual(testNode.FindChildByPosition(i), foundChildren[i]);
            }
        }

        /// <summary>
        /// Tests whether each position is accessible by ISyntaxNode::FindChildrenByType. In order for this test to work, all children of <paramref name="testNode"/> must be of type <typeparamref name="NODE_TYPE"/>.
        /// </summary>
        /// <typeparam name="NODE_TYPE">Type of node to find.</param>
        /// <param name="testNode">The node to test.</param>
        static private void Internal_FindChildrenByTypeGeneric_FindsAtAllPositions<NODE_TYPE>(NODE_TYPE testNode)
            where NODE_TYPE : class, ISyntaxNode
        {
            int childCount = testNode.CountChildren();

            NODE_TYPE[] foundChildren = testNode.FindChildrenByType<NODE_TYPE>();

            Assert.AreEqual(childCount, foundChildren.Length);

            for (int i = 0; i < childCount; ++i)
            {
                Assert.AreEqual(testNode.FindChildByPosition(i), foundChildren[i]);
            }
        }

        /// <summary>
        /// Tests whether each position is accessible by ISyntaxNode::FindChildrenByPredicate. In order for this test to work, all children of <paramref name="testNode"/> must satisfy <paramref name="predicate"/>.
        /// </summary>
        /// <param name="testNode">The node to test.</param>
        /// <param name="predicate">The predicate to find by.</param>
        static private void Internal_FindChildrenByPredicate_FindsAtAllPositions(ISyntaxNode testNode, Func<ISyntaxNode, bool> predicate)
        {
            int childCount = testNode.CountChildren();

            ISyntaxNode[] foundChildren = testNode.FindChildrenByPredicate(predicate);

            Assert.AreEqual(childCount, foundChildren.Length);

            for (int i = 0; i < childCount; ++i)
            {
                Assert.AreEqual(testNode.FindChildByPosition(i), foundChildren[i]);
            }
        }
        #endregion
    }
}
