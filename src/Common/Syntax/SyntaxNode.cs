﻿using System;
using System.Collections.Generic;

using Gotcha.Core;

namespace Gotcha.Common.Syntax
{
    abstract public class SyntaxNode : ISyntaxNode
    {
        public abstract int CountChildren();

        public abstract ISyntaxNode FindChildByPosition(int position);

        public ISyntaxNode FindChildByType(Type targetType, int searchStartPosition = 0)
        {
            int childCount = CountChildren();

            for (int i = searchStartPosition; i < childCount; ++i)
            {
                ISyntaxNode childNode = FindChildByPosition(i);

                if (targetType.IsAssignableFrom(childNode.GetType()))
                {
                    return childNode;
                }
            }

            return null;
        }

        public NODE_TYPE FindChildByType<NODE_TYPE>(int searchStartPosition = 0)
            where NODE_TYPE : class, ISyntaxNode
        {
            int childCount = CountChildren();

            for (int i = searchStartPosition; i < childCount; ++i)
            {
                NODE_TYPE childNode = FindChildByPosition(i) as NODE_TYPE;

                if (childNode != null)
                {
                    return childNode;
                }
            }

            return null;
        }

        public ISyntaxNode FindChildByPredicate(Func<ISyntaxNode, bool> predicate, int searchStartPosition = 0)
        {
            int childCount = CountChildren();

            for (int i = searchStartPosition; i < childCount; ++i)
            {
                ISyntaxNode childNode = FindChildByPosition(i);

                if (predicate.Invoke(childNode))
                {
                    return childNode;
                }
            }

            return null;
        }

        public ISyntaxNode[] FindChildrenByType(Type targetType)
        {
            List<ISyntaxNode> returnValue = new List<ISyntaxNode>();

            int childCount = CountChildren();

            for (int i = 0; i < childCount; ++i)
            {
                ISyntaxNode childNode = FindChildByPosition(i);

                if (targetType.IsAssignableFrom(childNode.GetType()))
                {
                    returnValue.Add(childNode);
                }
            }

            return returnValue.ToArray();
        }

        public NODE_TYPE[] FindChildrenByType<NODE_TYPE>()
            where NODE_TYPE : class, ISyntaxNode
        {
            List<NODE_TYPE> returnValue = new List<NODE_TYPE>();

            int childCount = CountChildren();

            for (int i = 0; i < childCount; ++i)
            {
                NODE_TYPE childNode = FindChildByPosition(i) as NODE_TYPE;

                if (childNode != null)
                {
                    returnValue.Add(childNode);
                }
            }

            return returnValue.ToArray();
        }

        public ISyntaxNode[] FindChildrenByPredicate(Func<ISyntaxNode, bool> predicate)
        {
            List<ISyntaxNode> returnValue = new List<ISyntaxNode>();

            int childCount = CountChildren();

            for (int i = 0; i < childCount; ++i)
            {
                ISyntaxNode childNode = FindChildByPosition(i);

                if (predicate.Invoke(childNode))
                {
                    returnValue.Add(childNode);
                }
            }

            return returnValue.ToArray();
        }

        abstract public void Emit(CodeBuilder code);

        public string Emit()
        {
            CodeBuilder code = new CodeBuilder();

            Emit(code);

            return code.ToString();
        }
    }
}
