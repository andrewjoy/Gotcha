﻿namespace Gotcha.Core.AssemblyInfo

open System.Reflection
open System.Runtime.InteropServices

[<assembly: AssemblyTitle("Gotcha.Core")>]
[<assembly: AssemblyDescription("Gotcha core systems.")>]
[<assembly: AssemblyCulture("")>]

[<assembly: ComVisible(false)>]

[<assembly: Guid("1437901c-2bfb-41c1-a6b5-783ca967b84a")>]

[<assembly: AssemblyVersion("0.1.0.0")>]
[<assembly: AssemblyFileVersion("0.1.0.0")>]

do
    ()
