﻿using Gotcha.Core;

namespace Gotcha.Common.Syntax
{
    sealed public class TokenArgumentSyntaxNode : ArgumentSyntaxNode
    {
        public enum ChildPosition
        {
            Token = 0
        }

        public TokenSyntaxNode Token { get; } = null;

        public TokenArgumentSyntaxNode(TokenSyntaxNode token)
        {
            this.Token = token;
        }

        public override int CountChildren()
        {
            return 1;
        }

        public override ISyntaxNode FindChildByPosition(int position)
        {
            if (position == 0)
            {
                return this.Token;
            }

            return null;
        }

        public ISyntaxNode FindChildByPosition(ChildPosition position)
        {
            return FindChildByPosition((int)position);
        }

        public override void Emit(CodeBuilder code)
        {
            this.Token.Emit(code);
        }
    }
}
