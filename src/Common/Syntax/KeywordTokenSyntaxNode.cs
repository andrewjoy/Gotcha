﻿using Gotcha.Core;

namespace Gotcha.Common.Syntax
{
    sealed public class KeywordTokenSyntaxNode : TokenSyntaxNode
    {
        public KeywordTokenSyntaxNode(string name)
            : base(name)
        {

        }

        public override void Emit(CodeBuilder code)
        {
            code.Append("keyword:" + this.Name);
        }
    }
}
