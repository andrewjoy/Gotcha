﻿namespace Gotcha.Core

open System
open System.Collections.Generic

/// <summary>
/// Simplifies working with sequences of nodes that must match required patterns.
/// </summary>
[<Sealed>]
type SyntaxPatternMatcher (nodes:ISyntaxNode[]) =
    let mutable frontNodeIndex:int = 0
    let mutable backNodeIndex:int = nodes.Length - 1
    
    /// <summary>
    /// Count of unmatched nodes remaining.
    /// </summary>
    /// <returns>Number of remaining nodes in the sequence.</returns>
    member this.RemainingNodes with get () : int = backNodeIndex - frontNodeIndex + 1

    /// <summary>
    /// Tests the front node in the sequence against the following requirements:
    /// - The node must share a type with or derive from <typeparamref name="NODE_TYPE"/>.
    /// </summary>
    /// <typeparam name="NODE_TYPE">Type to test node against.</typeparam>
    /// <returns>If the match is a success, the matched node will be returned.</returns>
    member this.MatchFront<'NODE_TYPE when 'NODE_TYPE : not struct and 'NODE_TYPE :> ISyntaxNode> () : Option<'NODE_TYPE> =
        if this.RemainingNodes > 0 then
            let result:Option<'NODE_TYPE> = this.MatchIndex<'NODE_TYPE> frontNodeIndex
            
            if result.IsSome then
                frontNodeIndex <- frontNodeIndex + 1

            result
        else
            None

    /// <summary>
    /// Tests the front node in the sequence against the following requirements:
    /// - The node must share a type with or derive from <typeparamref name="NODE_TYPE"/>.
    /// - The node must successfully pass <paramref name="predicate"/>.
    /// </summary>
    /// <typeparam name="NODE_TYPE">Type to test node against.</typeparam>
    /// <param name="predicate">Predicate to test node against.</param>
    /// <returns>If the match is a success, the matched node will be returned.</returns>
    member this.MatchFront<'NODE_TYPE when 'NODE_TYPE : not struct and 'NODE_TYPE :> ISyntaxNode> (predicate:Predicate<'NODE_TYPE>) : Option<'NODE_TYPE> =
        if this.RemainingNodes > 0 then
            let result:Option<'NODE_TYPE> = this.MatchIndex<'NODE_TYPE> (frontNodeIndex, predicate)
            
            if result.IsSome then
                frontNodeIndex <- frontNodeIndex + 1

            result
        else
            None

    /// <summary>
    /// Tests the back node in the sequence against the following requirements:
    /// - The node must share a type with or derive from <typeparamref name="NODE_TYPE"/>.
    /// </summary>
    /// <typeparam name="NODE_TYPE">Type to test node against.</typeparam>
    /// <returns>If the match is a success, the matched node will be returned.</returns>
    member this.MatchBack<'NODE_TYPE when 'NODE_TYPE : not struct and 'NODE_TYPE :> ISyntaxNode> () : Option<'NODE_TYPE> =
        if this.RemainingNodes > 0 then
            let result:Option<'NODE_TYPE> = this.MatchIndex<'NODE_TYPE> backNodeIndex
            
            if result.IsSome then
                backNodeIndex <- backNodeIndex - 1

            result
        else
            None

    /// <summary>
    /// Tests the back node in the sequence against the following requirements:
    /// - The node must share a type with or derive from <typeparamref name="NODE_TYPE"/>.
    /// - The node must successfully pass <paramref name="predicate"/>.
    /// </summary>
    /// <typeparam name="NODE_TYPE">Type to test node against.</typeparam>
    /// <param name="predicate">Predicate to test node against.</param>
    /// <returns>If the match is a success, the matched node will be returned.</returns>
    member this.MatchBack<'NODE_TYPE when 'NODE_TYPE : not struct and 'NODE_TYPE :> ISyntaxNode> (predicate:Predicate<'NODE_TYPE>) : Option<'NODE_TYPE> =
        if this.RemainingNodes > 0 then
            let result:Option<'NODE_TYPE> = this.MatchIndex<'NODE_TYPE> (backNodeIndex, predicate)
            
            if result.IsSome then
                backNodeIndex <- backNodeIndex - 1

            result
        else
            None

    /// <summary>
    /// Tests a number of nodes in the sequence, beginning with the front, against the following requirements:
    /// - The node must share a type with or derive from <typeparamref name="NODE_TYPE"/>.
    /// The number of nodes that successfully pass the tests must be equal to or greater than <paramref name="minimumRangeLength"/>.
    /// </summary>
    /// <typeparam name="NODE_TYPE">Type to test nodes against.</typeparam>
    /// <param name="minimumRangeLength">Required minimum number of nodes in the matched range.</param>
    /// <param name="maximumRangeLength">Maximum number of nodes to test.</param>
    /// <returns>If the match is a success, the matched node range will be returned.</returns>
    member this.MatchFrontRange<'NODE_TYPE when 'NODE_TYPE : not struct and 'NODE_TYPE :> ISyntaxNode> (minimumRangeLength:int, maximumRangeLength:int) : Option<'NODE_TYPE[]> =
        let rangeNodes:List<'NODE_TYPE> = List<'NODE_TYPE> (minimumRangeLength)

        let AddRangeNode (node:ISyntaxNode, rangeNodes:List<'NODE_TYPE>) : bool =
            match node with
            | :? 'NODE_TYPE as typedNode ->
                rangeNodes.Add typedNode

                true
            | _ ->
                false

        while this.RemainingNodes > rangeNodes.Count && rangeNodes.Count < maximumRangeLength && AddRangeNode (nodes.[frontNodeIndex + rangeNodes.Count], rangeNodes) do
            ()

        if rangeNodes.Count >= minimumRangeLength then
            frontNodeIndex <- frontNodeIndex + rangeNodes.Count
            
            Some (rangeNodes.ToArray ())
        else
            None
    
    /// <summary>
    /// Tests a number of nodes in the sequence, beginning with the front, against the following requirements:
    /// - The node must share a type with or derive from <typeparamref name="NODE_TYPE"/>.
    /// - The node must successfully pass <paramref name="predicate"/>.
    /// The number of nodes that successfully pass the tests must be equal to or greater than <paramref name="minimumRangeLength"/>.
    /// </summary>
    /// <typeparam name="NODE_TYPE">Type to test nodes against.</typeparam>
    /// <param name="minimumRangeLength">Required minimum number of nodes in the matched range.</param>
    /// <param name="maximumRangeLength">Maximum number of nodes to test.</param>
    /// <param name="predicate">Predicate to test nodes against.</param>
    /// <returns>If the match is a success, the matched node range will be returned.</returns>
    member this.MatchFrontRange<'NODE_TYPE when 'NODE_TYPE : not struct and 'NODE_TYPE :> ISyntaxNode> (minimumRangeLength:int, maximumRangeLength:int, predicate:Predicate<'NODE_TYPE>) : Option<'NODE_TYPE[]> =
        let rangeNodes:List<'NODE_TYPE> = List<'NODE_TYPE> (minimumRangeLength)

        let AddRangeNode (node:ISyntaxNode, rangeNodes:List<'NODE_TYPE>, predicate:Predicate<'NODE_TYPE>) : bool =
            match node with
            | :? 'NODE_TYPE as typedNode when predicate.Invoke typedNode ->
                rangeNodes.Add typedNode

                true
            | _ ->
                false

        while this.RemainingNodes > rangeNodes.Count && rangeNodes.Count < maximumRangeLength && AddRangeNode (nodes.[frontNodeIndex + rangeNodes.Count], rangeNodes, predicate) do
            ()

        if rangeNodes.Count >= minimumRangeLength then
            frontNodeIndex <- frontNodeIndex + rangeNodes.Count
            
            Some (rangeNodes.ToArray ())
        else
            None

    member private this.MatchIndex<'NODE_TYPE when 'NODE_TYPE : not struct and 'NODE_TYPE :> ISyntaxNode> (index:int) : Option<'NODE_TYPE> =
        match nodes.[index] with
        | :? 'NODE_TYPE as typedNode ->
            Some (typedNode)
        | _ ->
            None
    
    member private this.MatchIndex<'NODE_TYPE when 'NODE_TYPE : not struct and 'NODE_TYPE :> ISyntaxNode> (index:int, predicate:Predicate<'NODE_TYPE>) : Option<'NODE_TYPE> =
        match nodes.[index] with
        | :? 'NODE_TYPE as typedNode when predicate.Invoke typedNode ->
            Some (typedNode)
        | _ ->
            None