﻿using Gotcha.Core;

namespace Gotcha.Common.Syntax
{
    sealed public class OperatorTokenSyntaxNode : TokenSyntaxNode
    {
        public OperatorTokenSyntaxNode(string name)
            : base(name)
        {

        }

        public override void Emit(CodeBuilder code)
        {
            code.Append("operator:" + this.Name);
        }
    }
}
