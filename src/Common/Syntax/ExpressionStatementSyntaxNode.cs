﻿using Gotcha.Core;

namespace Gotcha.Common.Syntax
{
    sealed public class ExpressionStatementSyntaxNode : StatementSyntaxNode
    {
        public enum ChildPosition
        {
            Expression = 0
        }

        public ExpressionSyntaxNode Expression { get; } = null;

        public ExpressionStatementSyntaxNode(ExpressionSyntaxNode expression)
        {
            this.Expression = expression;
        }

        public override int CountChildren()
        {
            return 1;
        }

        public override ISyntaxNode FindChildByPosition(int position)
        {
            switch (position)
            {
                case 0:
                    return this.Expression;
            }

            return null;
        }

        public ISyntaxNode FindChildByPosition(ChildPosition position)
        {
            return FindChildByPosition((int)position);
        }

        public override void Emit(CodeBuilder code)
        {
            this.Expression.Emit(code);
        }
    }
}
