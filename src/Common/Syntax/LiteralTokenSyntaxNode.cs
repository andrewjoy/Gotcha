﻿using System;
using Gotcha.Core;

namespace Gotcha.Common.Syntax
{
    sealed public class LiteralTokenSyntaxNode : TokenSyntaxNode
    {
        public LiteralTokenSyntaxNode(string name)
            : base(name)
        {
            
        }

        public override void Emit(CodeBuilder code)
        {
            code.Append("literal:" + this.Name);
        }
    }
}
