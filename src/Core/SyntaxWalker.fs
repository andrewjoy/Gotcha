﻿namespace Gotcha.Core

open System

[<Sealed>]
[<AbstractClass>]
type SyntaxWalker private () =
    /// <summary>
    /// Finds the position of <paramref name="target"/> among its siblings.
    /// </summary>
    /// <param name="target">Child to search parent for.</param>
    /// <param name="parent">Parent to find child of.</param>
    static member FindPositionOf (target:ISyntaxNode, parent:ISyntaxNode) : int =
        let childCount:int = parent.CountChildren ()

        let rec FindAtOrAfter (position:int) : int =
            if position >= childCount then
                -1
            else
                if parent.FindChildByPosition position = target then
                    position
                else
                    FindAtOrAfter (position + 1)

        FindAtOrAfter 0

    /// <summary>
    /// Finds the position of <paramref name="target"/> among its siblings.
    /// </summary>
    /// <param name="target">Child to search parent for.</param>
    static member FindPositionOf (target:SyntaxScaffold) : int =
        let parent:SyntaxScaffold = target.Parent
        
        if parent <> null then
            SyntaxWalker.FindPositionOf (target.Node, parent.Node)
        else
            -1
    
    /// <summary>
    /// Invokes <paramref name="visitCallback"/> for every child of <paramref name="origin"/>, as well as itself.
    /// </summary>
    /// <param name="origin">Where to begin visitation from.</param>
    /// <param name="visitCallback">Callback function for each visit.</param>
    static member VisitDown (origin:SyntaxScaffold, visitCallback:Action<SyntaxScaffold>) : unit =
        visitCallback.Invoke origin
        
        for position = 0 to origin.ChildCount - 1 do
            SyntaxWalker.VisitDown (origin.GetChildAt position, visitCallback)

    /// <summary>
    /// Invokes <paramref name="visitCallback"/> for every child of <paramref name="origin"/>, as well as itself.
    /// </summary>
    /// <param name="origin">Where to begin visitation from.</param>
    /// <param name="visitCallback">Callback function for each visit.</param>
    static member VisitDown (origin:ISyntaxNode, visitCallback:Action<ISyntaxNode>) : unit =
        visitCallback.Invoke origin
        
        for position = 0 to (origin.CountChildren ()) - 1 do
            SyntaxWalker.VisitDown (origin.FindChildByPosition position, visitCallback)

    /// <summary>
    /// Invokes <paramref name="visitCallback"/> for every child of <paramref name="origin"/>, as well as itself, with a node that shares a type with or derives from <typeparamref name="NODE_TYPE"/>.
    /// </summary>
    /// <param name="targetType">Type of node to visit.</param>
    /// <param name="origin">Where to begin visitation from.</param>
    /// <param name="visitCallback">Callback function for each visit.</param>
    static member VisitDownWithType (targetType:Type, origin:SyntaxScaffold, visitCallback:Action<SyntaxScaffold>) : unit = 
        if targetType.IsAssignableFrom (origin.Node.GetType ()) then
            visitCallback.Invoke origin
        
        for position = 0 to origin.ChildCount - 1 do
            SyntaxWalker.VisitDownWithType (origin.GetChildAt position, visitCallback)

    /// <summary>
    /// Invokes <paramref name="visitCallback"/> for every child of <paramref name="origin"/>, as well as itself, with a node that shares a type with or derives from <typeparamref name="NODE_TYPE"/>.
    /// </summary>
    /// <typeparam name="NODE_TYPE">Type of node to visit.</typeparam>
    /// <param name="origin">Where to begin visitation from.</param>
    /// <param name="visitCallback">Callback function for each visit.</param>
    static member VisitDownWithType<'NODE_TYPE when 'NODE_TYPE : not struct and 'NODE_TYPE :> ISyntaxNode> (origin:SyntaxScaffold, visitCallback:Action<SyntaxScaffold>) : unit = 
        SyntaxWalker.VisitDownWithType (typeof<'NODE_TYPE>, origin, visitCallback)

    /// <summary>
    /// Invokes <paramref name="visitCallback"/> for every child of <paramref name="origin"/>, as well as itself, that shares a type with or derives from <typeparamref name="NODE_TYPE"/>.
    /// </summary>
    /// <param name="targetType">Type of node to visit.</param>
    /// <param name="origin">Where to begin visitation from.</param>
    /// <param name="visitCallback">Callback function for each visit.</param>
    static member VisitDownWithType (targetType:Type, origin:ISyntaxNode, visitCallback:Action<ISyntaxNode>) : unit = 
        if targetType.IsAssignableFrom (origin.GetType ()) then
            visitCallback.Invoke origin
        
        for position = 0 to (origin.CountChildren ()) - 1 do
            SyntaxWalker.VisitDownWithType (origin.FindChildByPosition position, visitCallback)

    /// <summary>
    /// Invokes <paramref name="visitCallback"/> for every child of <paramref name="origin"/>, as well as itself, that shares a type with or derives from <typeparamref name="NODE_TYPE"/>.
    /// </summary>
    /// <typeparam name="NODE_TYPE">Type of node to visit.</typeparam>
    /// <param name="origin">Where to begin visitation from.</param>
    /// <param name="visitCallback">Callback function for each visit.</param>
    static member VisitDownWithType<'NODE_TYPE when 'NODE_TYPE : not struct and 'NODE_TYPE :> ISyntaxNode> (origin:ISyntaxNode, visitCallback:Action<'NODE_TYPE>) : unit = 
        if origin :? 'NODE_TYPE then
            visitCallback.Invoke (origin :?> 'NODE_TYPE)
        
        for position = 0 to (origin.CountChildren ()) - 1 do
            SyntaxWalker.VisitDownWithType (origin.FindChildByPosition position, visitCallback)

    /// <summary>
    /// Invokes <paramref name="visitCallback"/> for every parent of <paramref name="origin"/>, as well as itself.
    /// </summary>
    /// <param name="origin">Where to begin visitation from.</param>
    /// <param name="visitCallback">Callback function for each visit.</param>
    static member VisitUp (origin:SyntaxScaffold, visitCallback:Action<SyntaxScaffold>) : unit =
        visitCallback.Invoke origin

        if origin.Parent <> null then
            SyntaxWalker.VisitUp (origin.Parent, visitCallback)
    
    /// <summary>
    /// Invokes <paramref name="visitCallback"/> for every parent of <paramref name="origin"/>, as well as itself, with a node that shares a type with or derives from <typeparamref name="NODE_TYPE"/>.
    /// </summary>
    /// <param name="targetType">Type of node to visit.</param>
    /// <param name="origin">Where to begin visitation from.</param>
    /// <param name="visitCallback">Callback function for each visit.</param>
    static member VisitUpWithType (targetType:Type, origin:SyntaxScaffold, visitCallback:Action<SyntaxScaffold>) : unit = 
        if targetType.IsAssignableFrom (origin.Node.GetType ()) then
            visitCallback.Invoke origin
        
        if origin.Parent <> null then
            SyntaxWalker.VisitUp (origin.Parent, visitCallback)
    
    /// <summary>
    /// Invokes <paramref name="visitCallback"/> for every parent of <paramref name="origin"/>, as well as itself, with a node that shares a type with or derives from <typeparamref name="NODE_TYPE"/>.
    /// </summary>
    /// <typeparam name="NODE_TYPE">Type of node to visit.</typeparam>
    /// <param name="origin">Where to begin visitation from.</param>
    /// <param name="visitCallback">Callback function for each visit.</param>
    static member VisitUpWithType<'NODE_TYPE when 'NODE_TYPE : not struct and 'NODE_TYPE :> ISyntaxNode> (origin:SyntaxScaffold, visitCallback:Action<SyntaxScaffold>) : unit = 
        SyntaxWalker.VisitUpWithType (typeof<'NODE_TYPE>, origin, visitCallback)
    
    /// <summary>
    /// Invokes <paramref name="visitCallback"/> for every sibling (children of parent) to the left (lower position) of <paramref name="origin"/>, as well as itself.
    /// </summary>
    /// <param name="origin">Where to begin visitation from.</param>
    /// <param name="visitCallback">Callback function for each visit.</param>
    static member VisitLeft (origin:SyntaxScaffold, visitCallback:Action<SyntaxScaffold>) : unit =
        visitCallback.Invoke origin

        let parent:SyntaxScaffold = origin.Parent

        if parent <> null then
            for position = SyntaxWalker.FindPositionOf (origin.Node, parent.Node) downto 0 do
                visitCallback.Invoke (parent.GetChildAt position)
    
    /// <summary>
    /// Invokes <paramref name="visitCallback"/> for every sibling (children of parent) to the left (lower position) of <paramref name="origin"/>, as well as itself, with a node that shares a type with or derives from <typeparamref name="NODE_TYPE"/>.
    /// </summary>
    /// <param name="targetType">Type of node to visit.</param>
    /// <param name="origin">Where to begin visitation from.</param>
    /// <param name="visitCallback">Callback function for each visit.</param>
    static member VisitLeftWithType (targetType:Type, origin:SyntaxScaffold, visitCallback:Action<SyntaxScaffold>) : unit =
        let parent:SyntaxScaffold = origin.Parent

        if parent <> null then
            for position = SyntaxWalker.FindPositionOf (origin.Node, parent.Node) downto 0 do
                let sibling:SyntaxScaffold = parent.GetChildAt position

                if targetType.IsAssignableFrom (sibling.Node.GetType ()) then
                    visitCallback.Invoke sibling

    /// <summary>
    /// Invokes <paramref name="visitCallback"/> for every sibling (children of parent) to the left (lower position) of <paramref name="origin"/>, as well as itself, with a node that shares a type with or derives from <typeparamref name="NODE_TYPE"/>.
    /// </summary>
    /// <typeparam name="NODE_TYPE">Type of node to visit.</typeparam>
    /// <param name="origin">Where to begin visitation from.</param>
    /// <param name="visitCallback">Callback function for each visit.</param>
    static member VisitLeftWithType<'NODE_TYPE when 'NODE_TYPE : not struct and 'NODE_TYPE :> ISyntaxNode> (origin:SyntaxScaffold, visitCallback:Action<SyntaxScaffold>) : unit =
        SyntaxWalker.VisitLeftWithType (typeof<'NODE_TYPE>, origin, visitCallback)

    /// <summary>
    /// Invokes <paramref name="visitCallback"/> for every sibling (children of parent) to the right (higher position) of <paramref name="origin"/>, as well as itself.
    /// </summary>
    /// <param name="origin">Where to begin visitation from.</param>
    /// <param name="visitCallback">Callback function for each visit.</param>
    static member VisitRight (origin:SyntaxScaffold, visitCallback:Action<SyntaxScaffold>) : unit =
        visitCallback.Invoke origin

        let parent:SyntaxScaffold = origin.Parent

        if parent <> null then
            for position = SyntaxWalker.FindPositionOf (origin.Node, parent.Node) to parent.ChildCount - 1 do
                visitCallback.Invoke (parent.GetChildAt position)
    
    /// <summary>
    /// Invokes <paramref name="visitCallback"/> for every sibling (children of parent) to the right (higher position) of <paramref name="origin"/>, as well as itself, with a node that shares a type with or derives from <typeparamref name="NODE_TYPE"/>.
    /// </summary>
    /// <param name="targetType">Type of node to visit.</param>
    /// <param name="origin">Where to begin visitation from.</param>
    /// <param name="visitCallback">Callback function for each visit.</param>
    static member VisitRightWithType (targetType:Type, origin:SyntaxScaffold, visitCallback:Action<SyntaxScaffold>) : unit =
        let parent:SyntaxScaffold = origin.Parent

        if parent <> null then
            for position = SyntaxWalker.FindPositionOf (origin.Node, parent.Node) to parent.ChildCount - 1 do
                let sibling:SyntaxScaffold = parent.GetChildAt position

                if targetType.IsAssignableFrom (sibling.Node.GetType ()) then
                    visitCallback.Invoke sibling

    /// <summary>
    /// Invokes <paramref name="visitCallback"/> for every sibling (children of parent) to the right (higher position) of <paramref name="origin"/>, as well as itself, with a node that shares a type with or derives from <typeparamref name="NODE_TYPE"/>.
    /// </summary>
    /// <typeparam name="NODE_TYPE">Type of node to visit.</typeparam>
    /// <param name="origin">Where to begin visitation from.</param>
    /// <param name="visitCallback">Callback function for each visit.</param>
    static member VisitRightWithType<'NODE_TYPE when 'NODE_TYPE : not struct and 'NODE_TYPE :> ISyntaxNode> (origin:SyntaxScaffold, visitCallback:Action<SyntaxScaffold>) : unit =
        SyntaxWalker.VisitRightWithType (typeof<'NODE_TYPE>, origin, visitCallback)

    /// <summary>
    /// Invokes <paramref name="visitCallback"/> for every sibling (children of parent) of <paramref name="origin"/>, as well as itself.
    /// </summary>
    /// <param name="origin">Where to begin visitation from.</param>
    /// <param name="visitCallback">Callback function for each visit.</param>
    static member VisitLeftAndRight (origin:SyntaxScaffold, visitCallback:Action<SyntaxScaffold>) : unit =
        visitCallback.Invoke origin

        let parent:SyntaxScaffold = origin.Parent

        if parent <> null then
            for position = 0 to parent.ChildCount - 1 do
                visitCallback.Invoke (parent.GetChildAt position)
    
    /// <summary>
    /// Invokes <paramref name="visitCallback"/> for every sibling (children of parent) of <paramref name="origin"/>, as well as itself, with a node that shares a type with or derives from <typeparamref name="NODE_TYPE"/>.
    /// </summary>
    /// <param name="targetType">Type of node to visit.</param>
    /// <param name="origin">Where to begin visitation from.</param>
    /// <param name="visitCallback">Callback function for each visit.</param>
    static member VisitLeftAndRightWithType (targetType:Type, origin:SyntaxScaffold, visitCallback:Action<SyntaxScaffold>) : unit =
        let parent:SyntaxScaffold = origin.Parent

        if parent <> null then
            for position = 0 to parent.ChildCount - 1 do
                let sibling:SyntaxScaffold = parent.GetChildAt position

                if targetType.IsAssignableFrom (sibling.Node.GetType ()) then
                    visitCallback.Invoke sibling

    /// <summary>
    /// Invokes <paramref name="visitCallback"/> for every sibling (children of parent) of <paramref name="origin"/>, as well as itself, with a node that shares a type with or derives from <typeparamref name="NODE_TYPE"/>.
    /// </summary>
    /// <typeparam name="NODE_TYPE">Type of node to visit.</typeparam>
    /// <param name="origin">Where to begin visitation from.</param>
    /// <param name="visitCallback">Callback function for each visit.</param>
    static member VisitLeftAndRightWithType<'NODE_TYPE when 'NODE_TYPE : not struct and 'NODE_TYPE :> ISyntaxNode> (origin:SyntaxScaffold, visitCallback:Action<SyntaxScaffold>) : unit =
        SyntaxWalker.VisitLeftAndRightWithType (typeof<'NODE_TYPE>, origin, visitCallback)