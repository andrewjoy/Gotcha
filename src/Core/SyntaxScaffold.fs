﻿namespace Gotcha.Core

open System
open System.Collections.Generic

/// <summary>
/// Provides two-way traversal to and from children of a syntax node. WARNING: Parents are stored as weak references, so be sure to hold the root or it will be garbage collected to your current reference.
/// </summary>
[<Sealed>]
[<AllowNullLiteral>]
type SyntaxScaffold (node:ISyntaxNode, parent:WeakReference<SyntaxScaffold>, depth:int) as this =
    let children:SyntaxScaffold[] = [| for position in 0 .. (node.CountChildren () - 1) -> SyntaxScaffold (node.FindChildByPosition position, WeakReference<SyntaxScaffold> this, depth + 1) |]     
    
    new (node:ISyntaxNode) =
        SyntaxScaffold (node, WeakReference<SyntaxScaffold> null, 0)

    member this.Node with get () : ISyntaxNode = node

    member this.Parent 
        with get () : SyntaxScaffold = 
            let mutable returnValue:SyntaxScaffold = null

            ignore (parent.TryGetTarget (&returnValue))

            returnValue

    member this.Depth with get () : int = depth

    member this.ChildCount with get () : int = children.Length
    member this.GetChildAt (position:int) : SyntaxScaffold = children.[position]

    /// <summary>
    /// Retrieves the first immediate child scaffold with node of type <paramref name="targetType"/>.
    /// </summary>
    /// <param name="targetType">Type of scaffold node to find.</param>
    /// <param name="searchStartPosition">Position to start searching from.</param>
    /// <returns>First child scaffold found with node that matches or derives from <paramref name="targetType"/>.</returns>
    member this.FindChildByType (targetType:Type, searchStartPosition:int) : SyntaxScaffold =
        let mutable currentSearchPosition:int = searchStartPosition
        let mutable returnValue:SyntaxScaffold = Unchecked.defaultof<SyntaxScaffold>

        while currentSearchPosition < this.ChildCount do
            let currentSearchChild:SyntaxScaffold = this.GetChildAt currentSearchPosition

            if targetType.IsAssignableFrom (currentSearchChild.Node.GetType ()) then
                returnValue <- currentSearchChild

                currentSearchPosition <- this.ChildCount - 1

            currentSearchPosition <- currentSearchPosition + 1

        returnValue

    /// <summary>
    /// Retrieves the first immediate child scaffold with node of type <paramref name="targetType"/>.
    /// </summary>
    /// <param name="targetType">Type of scaffold node to find.</param>
    /// <returns>First child scaffold found with node that matches or derives from <paramref name="targetType"/>.</returns>
    member this.FindChildByType (targetType:Type) : SyntaxScaffold =
        this.FindChildByType (targetType, 0)

    /// <summary>
    /// Retrieves the first immediate child scaffold with node of type <typeparamref name="NODE_TYPE"/>.
    /// </summary>
    /// <typeparam name="NODE_TYPE">Type of scaffold node to find.</typeparam>
    /// <param name="searchStartPosition">Position to start searching from.</param>
    /// <returns>First child scaffold found with node that matches or derives from <typeparamref name="NODE_TYPE"/>.</returns>
    member this.FindChildByType<'NODE_TYPE when 'NODE_TYPE : not struct and 'NODE_TYPE :> ISyntaxNode> (searchStartPosition:int) : SyntaxScaffold =
        this.FindChildByType (typeof<'NODE_TYPE>, searchStartPosition)

    /// <summary>
    /// Retrieves the first immediate child scaffold with node of type <typeparamref name="NODE_TYPE"/>.
    /// </summary>
    /// <typeparam name="NODE_TYPE">Type of scaffold node to find.</typeparam>
    /// <returns>First child scaffold found with node that matches or derives from <typeparamref name="NODE_TYPE"/>.</returns>
    member this.FindChildByType<'NODE_TYPE when 'NODE_TYPE : not struct and 'NODE_TYPE :> ISyntaxNode> () : SyntaxScaffold =
        this.FindChildByType<'NODE_TYPE> 0

    /// <summary>
    /// Retrieves the first immediate child that satisfies <paramref name="predicate"/>.
    /// </summary>
    /// <param name="predicate">Predicate to test parents against.</param>
    /// <param name="searchStartPosition">Position to start searching from.</param>
    /// <returns>First child scaffold found that satisfies <paramref name="predicate"/>.</returns>
    member this.FindChildByPredicate (predicate:Func<SyntaxScaffold, bool>, searchStartPosition:int) : SyntaxScaffold =
        let mutable currentSearchPosition:int = searchStartPosition
        let mutable returnValue:SyntaxScaffold = Unchecked.defaultof<SyntaxScaffold>

        while currentSearchPosition < this.ChildCount do
            let currentSearchChild:SyntaxScaffold = this.GetChildAt currentSearchPosition

            if predicate.Invoke currentSearchChild then
                returnValue <- currentSearchChild

                currentSearchPosition <- this.ChildCount - 1

            currentSearchPosition <- currentSearchPosition + 1

        returnValue

    /// <summary>
    /// Retrieves the first immediate child that satisfies <paramref name="predicate"/>.
    /// </summary>
    /// <param name="predicate">Predicate to test child scaffolds against.</param>
    /// <returns>First child scaffold found that satisfies <paramref name="predicate"/>.</returns>
    member this.FindChildByPredicate (predicate:Func<SyntaxScaffold, bool>) : SyntaxScaffold =
        this.FindChildByPredicate (predicate, 0)

    /// <summary>
    /// Retrieves all immediate child scaffolds with node of type <paramref name="targetType"/>.
    /// </summary>
    /// <param name="targetType">Type of scaffold node to find.</param>
    /// <returns>An array of child scaffolds found with a node that matches or derives from <paramref name="targetType"/>.</returns>
    member this.FindChildrenByType (targetType:Type) : SyntaxScaffold[] =
        let returnValue:List<SyntaxScaffold> = List<SyntaxScaffold> (this.ChildCount)

        for childPosition = 0 to this.ChildCount - 1 do
            let child:SyntaxScaffold = this.GetChildAt childPosition

            if targetType.IsAssignableFrom (child.Node.GetType ()) then
                returnValue.Add child

        returnValue.ToArray ()

    /// <summary>
    /// Retrieves all immediate child scaffolds with node of type <typeparamref name="NODE_TYPE"/>.
    /// </summary>
    /// <typeparam name="NODE_TYPE">Type of scaffold node to find.</typeparam>
    /// <returns>An array of child scaffolds found with a node that matches or derives from <typeparamref name="NODE_TYPE"/>.</returns>
    member this.FindChildrenByType<'NODE_TYPE when 'NODE_TYPE : not struct and 'NODE_TYPE :> ISyntaxNode> () : SyntaxScaffold[] =
        this.FindChildrenByType typeof<'NODE_TYPE>

    /// <summary>
    /// Retrieves all immediate child scaffolds that satisfy <paramref name="predicate"/>.
    /// </summary>
    /// <param name="predicate">Predicate to test child scaffolds against.</param>
    /// <returns>An array of child scaffolds found that satisfy <paramref name="predicate"/>.</returns>
    member this.FindChildrenByPredicate (predicate:Func<SyntaxScaffold, bool>) : SyntaxScaffold[] =
        let returnValue:List<SyntaxScaffold> = List<SyntaxScaffold> (this.ChildCount)

        for childPosition = 0 to this.ChildCount - 1 do
            let child:SyntaxScaffold = this.GetChildAt childPosition

            if predicate.Invoke child then
                returnValue.Add child

        returnValue.ToArray ()

    /// <summary>
    /// Retrieves the first parent scaffold with node of type <paramref name="targetType"/>.
    /// </summary>
    /// <param name="targetType">Type of scaffold node to find.</param>
    /// <returns>First parent scaffold found with node that matches or derives from <paramref name="targetType"/>.</returns>
    member this.FindParentByType (targetType:Type) : SyntaxScaffold =
        match this.Parent with
        | null -> null
        | parent when targetType.IsAssignableFrom (parent.Node.GetType ()) -> parent
        | parent -> parent.FindParentByType targetType

    /// <summary>
    /// Retrieves the first parent scaffold with node of type <typeparamref name="NODE_TYPE"/>.
    /// </summary>
    /// <typeparam name="NODE_TYPE">Type of scaffold node to find.</typeparam>
    /// <returns>First parent scaffold found with node that matches or derives from <typeparamref name="NODE_TYPE"/>.</returns>
    member this.FindParentByType<'NODE_TYPE when 'NODE_TYPE : not struct and 'NODE_TYPE :> ISyntaxNode> () : SyntaxScaffold =
        this.FindParentByType typeof<'NODE_TYPE>

    /// <summary>
    /// Retrieves the first parent that satisfies <paramref name="predicate"/>.
    /// </summary>
    /// <param name="predicate">Predicate to test parent scaffolds against.</param>
    /// <returns>First parent scaffold found that satisfies <paramref name="predicate"/>.</returns>
    member this.FindParentByPredicate (predicate:Func<SyntaxScaffold, bool>) : SyntaxScaffold =
        match this.Parent with
        | null -> null
        | parent when predicate.Invoke parent -> parent
        | parent -> parent.FindParentByPredicate predicate

    /// <summary>
    /// Retrieves all parent scaffolds with node of type <paramref name="targetType"/>.
    /// </summary>
    /// <param name="targetType">Type of scaffold node to find.</param>
    /// <returns>An array of parent scaffolds found with a node that matches or derives from <paramref name="targetType"/>.</returns>
    member this.FindParentsByType (targetType:Type) : SyntaxScaffold[] =
        let returnValue:List<SyntaxScaffold> = List<SyntaxScaffold> ()

        let mutable currentParent:SyntaxScaffold = this.Parent

        while currentParent <> null do
            if targetType.IsAssignableFrom (currentParent.Node.GetType ()) then
                returnValue.Add currentParent

            currentParent <- currentParent.Parent

        returnValue.ToArray ()

    /// <summary>
    /// Retrieves all parent scaffolds with node of type <typeparamref name="NODE_TYPE"/>.
    /// </summary>
    /// <typeparam name="NODE_TYPE">Type of scaffold node to find.</typeparam>
    /// <returns>An array of parent scaffolds found with a node that matches or derives from <typeparamref name="NODE_TYPE"/>.</returns>
    member this.FindParentsByType<'NODE_TYPE when 'NODE_TYPE : not struct and 'NODE_TYPE :> ISyntaxNode> () : SyntaxScaffold[] =
        this.FindParentsByType typeof<'NODE_TYPE>

    /// <summary>
    /// Retrieves all parent scaffolds that satisfy <paramref name="predicate"/>.
    /// </summary>
    /// <param name="predicate">Predicate to test parent scaffolds against.</param>
    /// <returns>An array of parent scaffolds found that satisfy <paramref name="predicate"/>.</returns>
    member this.FindParentsByPredicate (predicate:Func<SyntaxScaffold, bool>) : SyntaxScaffold[] =
        let returnValue:List<SyntaxScaffold> = List<SyntaxScaffold> ()
        
        let mutable currentParent:SyntaxScaffold = this.Parent

        while currentParent <> null do
            if predicate.Invoke currentParent then
                returnValue.Add currentParent

            currentParent <- currentParent.Parent

        returnValue.ToArray ()