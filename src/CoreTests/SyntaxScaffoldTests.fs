﻿namespace Gotcha.CoreTests

open System

open Gotcha.Core

open NUnit.Framework

[<TestFixture>]
type SyntaxScaffoldTests () =
    [<Test>]
    static member Construction_TouchesAllNodes () : unit =
        let children:ISyntaxNode[] = [| DummySyntaxNode (); DummySyntaxNode () |]
        let parent:ISyntaxNode = DummySyntaxNode (children) :> ISyntaxNode
        
        let testScaffold:SyntaxScaffold = SyntaxScaffold parent

        Assert.AreEqual (children.Length, testScaffold.ChildCount)

        Assert.AreEqual (parent, testScaffold.Node)
        Assert.AreEqual (children.[0], (testScaffold.GetChildAt 0).Node)
        Assert.AreEqual (children.[1], (testScaffold.GetChildAt 1).Node)

    [<Test>]
    static member FindChildByType_FindsAtAllPositions () : unit =
        let rootScaffold:SyntaxScaffold = SyntaxScaffold (DummySyntaxNode ([| DummySyntaxNode (); DummySyntaxNode (); |]))

        Assert.IsNull (rootScaffold.FindChildByType typeof<SecondaryDummySyntaxNode>)

        for childPosition = 0 to rootScaffold.ChildCount - 1 do
            Assert.AreEqual (rootScaffold.GetChildAt childPosition, rootScaffold.FindChildByType (typeof<DummySyntaxNode>, childPosition))

    [<Test>]
    static member FindChildByTypeGeneric_FindsAtAllPositions () : unit =
        let rootScaffold:SyntaxScaffold = SyntaxScaffold (DummySyntaxNode ([| DummySyntaxNode (); DummySyntaxNode (); |]))

        Assert.IsNull (rootScaffold.FindChildByType<SecondaryDummySyntaxNode> ())

        for childPosition = 0 to rootScaffold.ChildCount - 1 do
            Assert.AreEqual (rootScaffold.GetChildAt childPosition, rootScaffold.FindChildByType<DummySyntaxNode> childPosition)

    [<Test>]
    static member FindChildByPredicate_FindsAtAllPositions () : unit =
        let rootScaffold:SyntaxScaffold = SyntaxScaffold (DummySyntaxNode ([| DummySyntaxNode (); DummySyntaxNode (); |]))

        Assert.IsNull (rootScaffold.FindChildByPredicate (fun (scaffold:SyntaxScaffold) -> scaffold.ChildCount = 2))

        for childPosition = 0 to rootScaffold.ChildCount - 1 do
            Assert.AreEqual (rootScaffold.GetChildAt childPosition, rootScaffold.FindChildByPredicate ((fun (scaffold:SyntaxScaffold) -> scaffold.ChildCount = 0), childPosition))
    
    [<Test>]
    static member FindChildrenByType_FindsAtAllPositions () : unit =
        let rootScaffold:SyntaxScaffold = SyntaxScaffold (DummySyntaxNode ([| DummySyntaxNode (); DummySyntaxNode (); |]))

        Assert.AreEqual (0, (rootScaffold.FindChildrenByType typeof<SecondaryDummySyntaxNode>).Length)

        let foundChildren:SyntaxScaffold[] = rootScaffold.FindChildrenByType typeof<DummySyntaxNode>

        Assert.AreEqual (rootScaffold.ChildCount, foundChildren.Length)

        for childPosition = 0 to rootScaffold.ChildCount - 1 do
            Assert.AreEqual (rootScaffold.GetChildAt childPosition, foundChildren.[childPosition])

    [<Test>]
    static member FindChildrenByTypeGeneric_FindsAtAllPositions () : unit =
        let rootScaffold:SyntaxScaffold = SyntaxScaffold (DummySyntaxNode ([| DummySyntaxNode (); DummySyntaxNode (); |]))

        Assert.AreEqual (0, (rootScaffold.FindChildrenByType<SecondaryDummySyntaxNode> ()).Length)

        let foundChildren:SyntaxScaffold[] = rootScaffold.FindChildrenByType<DummySyntaxNode> ()

        Assert.AreEqual (rootScaffold.ChildCount, foundChildren.Length)

        for childPosition = 0 to rootScaffold.ChildCount - 1 do
            Assert.AreEqual (rootScaffold.GetChildAt childPosition, foundChildren.[childPosition])

    [<Test>]
    static member FindChildrenByPredicate_FindsAtAllPositions () : unit =
        let rootScaffold:SyntaxScaffold = SyntaxScaffold (DummySyntaxNode ([| DummySyntaxNode (); DummySyntaxNode (); |]))

        Assert.AreEqual (0, (rootScaffold.FindChildrenByPredicate (fun (scaffold:SyntaxScaffold) -> scaffold.ChildCount = 2)).Length)

        let foundChildren:SyntaxScaffold[] = rootScaffold.FindChildrenByPredicate (fun (scaffold:SyntaxScaffold) -> scaffold.ChildCount = 0)

        Assert.AreEqual (rootScaffold.ChildCount, foundChildren.Length)

        for childPosition = 0 to rootScaffold.ChildCount - 1 do
            Assert.AreEqual (rootScaffold.GetChildAt childPosition, foundChildren.[childPosition])

    [<Test>]
    static member FindParentByType_FindsAtAllDepths () : unit =
        let testDepth:int = 2
        
        let rootScaffold:SyntaxScaffold = SyntaxScaffold (SyntaxScaffoldTests.Internal_CreateSyntaxNodeChain testDepth)

        let mutable testResult:SyntaxScaffold = (SyntaxScaffoldTests.Internal_GetScaffoldChildAtDepth (rootScaffold, testDepth))

        for depth in (testDepth - 1) .. 0 do
            testResult <- testResult.FindParentByType typeof<DummySyntaxNode>

            Assert.NotNull testResult
            Assert.AreEqual (depth, testResult.Depth)

    [<Test>]
    static member FindParentByPredicate_FindsAtAllDepths () : unit =
        let testDepth:int = 6
        
        let rootScaffold:SyntaxScaffold = SyntaxScaffold (SyntaxScaffoldTests.Internal_CreateSyntaxNodeChain testDepth)

        let mutable testResult:SyntaxScaffold = (SyntaxScaffoldTests.Internal_GetScaffoldChildAtDepth (rootScaffold, testDepth))

        let predicate:Func<SyntaxScaffold, bool> = Func<SyntaxScaffold, bool> (fun (scaffold:SyntaxScaffold) -> (scaffold.Depth % 2) = 0);

        testResult <- testResult.FindParentByPredicate predicate

        while testResult <> null do
            Assert.True (predicate.Invoke testResult)

            testResult <- testResult.FindParentByPredicate predicate

    [<Test>]
    static member FindParentsByType_FindsAtAllDepths () : unit =
        let testDepth:int = 2

        let rootScaffold:SyntaxScaffold = SyntaxScaffold (SyntaxScaffoldTests.Internal_CreateSyntaxNodeChain (testDepth + 1))

        let testResult:SyntaxScaffold[] = (SyntaxScaffoldTests.Internal_GetScaffoldChildAtDepth (rootScaffold, testDepth)).FindParentsByType typeof<DummySyntaxNode>

        Assert.AreEqual (testResult.Length, testDepth)

        for position = 0 to (testResult.Length - 1) do
            Assert.AreEqual (testResult.Length - 1 - position, testResult.[position].Depth)

    [<Test>]
    static member FindParentsByPredicate_FindsAtAllDepths () : unit =
        let testDepth:int = 2

        let rootScaffold:SyntaxScaffold = SyntaxScaffold (SyntaxScaffoldTests.Internal_CreateSyntaxNodeChain (testDepth + 1))

        let predicate:Func<SyntaxScaffold, bool> = Func<SyntaxScaffold, bool> (fun (scaffold:SyntaxScaffold) -> (scaffold.Depth % 2) = 0);

        let testResult:SyntaxScaffold[] = (SyntaxScaffoldTests.Internal_GetScaffoldChildAtDepth (rootScaffold, testDepth)).FindParentsByPredicate predicate

        for position = 0 to (testResult.Length - 1) do
            Assert.True (predicate.Invoke testResult.[position])

    /// <summary>
    /// Creates a tree of single-child syntax nodes with a depth of <paramref name="depth">.
    /// </summary>
    /// <param name="depth">Depth of returned tree.</param>
    /// <returns>Constructed tree.</returns>
    static member private Internal_CreateSyntaxNodeChain (depth:int) : DummySyntaxNode =
        if depth = 0 then
            DummySyntaxNode ()
        else
            DummySyntaxNode [| SyntaxScaffoldTests.Internal_CreateSyntaxNodeChain (depth - 1) |]

    /// <summary>
    /// Returns child of <paramref name="scaffold"> at <paramref name="depth"> and position 0.
    /// </summary>
    /// <param name="scaffold">Scaffold to traverse from.</param>
    /// <param name="depth">Depth of returned node.</param>
    /// <returns>Child scaffold at target depth.</returns>
    static member private Internal_GetScaffoldChildAtDepth (scaffold:SyntaxScaffold, depth:int) : SyntaxScaffold =
        if depth = 0 then
            scaffold
        else
            SyntaxScaffoldTests.Internal_GetScaffoldChildAtDepth (scaffold.GetChildAt 0, depth - 1)