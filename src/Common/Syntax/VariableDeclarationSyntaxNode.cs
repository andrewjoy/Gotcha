﻿namespace Gotcha.Common.Syntax
{
    sealed public class VariableDeclarationSyntaxNode : DeclarationSyntaxNode
    {
        public VariableDeclarationSyntaxNode(ModifierListSyntaxNode modifiers, IdentifierTokenSyntaxNode identifier)
            : base(modifiers, identifier)
        {

        }
    }
}
