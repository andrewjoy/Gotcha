Gotcha
======

Crowdsourced code cleanliness.

Contributing
============

Setup
-----

1. ```git clone``` the repository
2. Open ```./src/Gotcha.sln``` in Visual Studio
3. Right-click the solution and open ```Manage NuGet Packages for Solution...```
4. Restore missing packages
5. Build the solution

Requirements
------------

- Visual Studio 2015
    - .Net 4.6
