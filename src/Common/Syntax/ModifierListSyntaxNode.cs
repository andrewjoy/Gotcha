﻿using Gotcha.Core;

namespace Gotcha.Common.Syntax
{
    sealed public class ModifierListSyntaxNode : SyntaxNode
    {
        private readonly ModifierSyntaxNode[] modifiers = null;

        public ModifierListSyntaxNode(ModifierSyntaxNode[] modifiers)
        {
            this.modifiers = modifiers;
        }

        public override int CountChildren()
        {
            return this.modifiers.Length;
        }

        public override ISyntaxNode FindChildByPosition(int position)
        {
            if (position >= 0 && position < this.modifiers.Length)
            {
                return this.modifiers[position];
            }

            return null;
        }

        public override void Emit(CodeBuilder code)
        {
            for (int i = 0; i < this.modifiers.Length; ++i)
            {
                this.modifiers[i].Emit(code);

                code.Append(" ");
            }
        }
    }
}
