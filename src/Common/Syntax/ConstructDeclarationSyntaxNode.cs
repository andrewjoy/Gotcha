﻿using Gotcha.Core;

namespace Gotcha.Common.Syntax
{
    abstract public class ConstructDeclarationSyntaxNode : DeclarationSyntaxNode
    {
        public ConstructDeclarationSyntaxNode(ModifierListSyntaxNode modifiers, IdentifierTokenSyntaxNode identifier)
            :  base(modifiers, identifier)
        {

        }
    }

    sealed public class ConstructDeclarationSyntaxNode<CONSTRUCTTYPE_TYPE> : ConstructDeclarationSyntaxNode
        where CONSTRUCTTYPE_TYPE : struct
    {
        public CONSTRUCTTYPE_TYPE ConstructType { get; } = default(CONSTRUCTTYPE_TYPE);

        public ConstructDeclarationSyntaxNode(CONSTRUCTTYPE_TYPE constructType, ModifierListSyntaxNode modifiers, IdentifierTokenSyntaxNode identifier)
            : base(modifiers, identifier)
        {
            this.ConstructType = constructType;
        }

        public override void Emit(CodeBuilder code)
        {
            base.Emit(code);

            code.Append(" with construct_type " + ConstructType);
        }
    }
}
