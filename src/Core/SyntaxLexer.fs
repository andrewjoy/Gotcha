﻿namespace Gotcha.Core

open System
open System.Linq
open System.Collections.Generic

type SyntaxLexeme = string

[<Sealed>]
type SyntaxLexemeRange (startLexeme:SyntaxLexeme, endLexeme:SyntaxLexeme, discardStartAndEnd:bool) =
    member this.Start with get () : SyntaxLexeme = startLexeme
    member this.End with get () : SyntaxLexeme = endLexeme
    member this.DiscardStartAndEnd with get () : bool = discardStartAndEnd

type SyntaxTokenKind =
| IdentifierOrLiteral = 0
| Keyword = 1
| Operator = 2

[<Sealed>]
[<AllowNullLiteral>]
type SyntaxToken (lexeme:SyntaxLexeme, kind:SyntaxTokenKind) =
    member this.Lexeme with get () : SyntaxLexeme = lexeme
    member this.Kind with get () : SyntaxTokenKind = kind

/// <summary>
/// Discriminated union that represents a tree of prioritised token groups.
/// </summary>
type SyntaxTokenHeirarchy =
| Leaf of Token:SyntaxToken
| Parent of Children:SyntaxTokenHeirarchy[]

type SyntaxTokenHeirarchy with
    /// <summary>
    /// Retrieves all heirarchies that compose this heirarchy (children, and/or itself if it contains meaningful data).
    /// </summary>
    /// <returns>Array of significant component heirarchies.</returns>
    member this.GetComponentHeirarchies () : SyntaxTokenHeirarchy[] =
        match this with
        | Leaf _ -> [| this |]
        | Parent children -> children

/// <summary>
/// General purpose tokenizer with derivable pipeline stages and modifyable rules.
/// </summary>
type SyntaxLexer () =
    let lexemeSeparators:List<char> = List<char> ()

    let splitterLexemes:List<SyntaxLexeme> = List<SyntaxLexeme> ()
    let combinerLexemeRanges:List<SyntaxLexemeRange> = List<SyntaxLexemeRange> ()
    let reservedLexemes:Dictionary<SyntaxLexeme, SyntaxTokenKind> = Dictionary<SyntaxLexeme, SyntaxTokenKind> ()
    let grouperLexemeRanges:List<SyntaxLexemeRange> = List<SyntaxLexemeRange> ()

    static member MatchTextAgainstSource (text:string, source:string, sourceIndex:int) : bool =
        let rec CompareFromIndex (textIndex:int) : bool =
            if textIndex >= text.Length then
                true
            else
                if text.[textIndex] = source.[sourceIndex + textIndex] then
                    CompareFromIndex (textIndex + 1)
                else
                    false

        if source.Length < sourceIndex + text.Length then
            false
        else
            CompareFromIndex 0

    /// <summary>
    /// Characters used to separate distinct lexemes (ex. ' ').
    /// </summary>
    member this.LexemeSeparators with get () : IEnumerable<char> = lexemeSeparators :> IEnumerable<char>

    /// <summary>
    /// Lexemes that can be considered distinct while being adjacent to other lexemes (ex. '(', '.', '=').
    /// Order is important because of first-come first-served splitting. Splitter lexemes will be sorted by length to ensure accurate splitting ('<-' vs '<', '-').
    /// Splitter lexemes will not themselves be split.
    /// </summary>
    member this.SplitterLexemes with get () : IEnumerable<SyntaxLexeme> = splitterLexemes :> IEnumerable<SyntaxLexeme>

    /// <summary>
    /// Lexemes that should combine with subsequent lexemes into a single lexeme (ex. '", "').
    /// The second lexeme will end the combining process.
    ///   - If it is null, the combining will continue until the end of the statement.
    ///   - If it is the same as the first value, combination will end on a second occurance of the lexeme.
    /// Order is important because combiner lexemes will be ignored if combined by another lexeme first. Combiner lexemes will be sorted by length to ensure accurate splitting ('""", """' vs '", "').
    /// Combined lexemes will skip the 'splitting' stage.
    /// </summary>
    member this.CombinerLexemeRanges with get () : IEnumerable<SyntaxLexemeRange> = combinerLexemeRanges :> IEnumerable<SyntaxLexemeRange>

    /// <summary>
    /// Lexemes with specific kinds other than identifiers and literals, such as keywords and operators. Used during transformation.
    /// </summary>
    member this.ReservedLexemes with get () : IEnumerable<KeyValuePair<SyntaxLexeme, SyntaxTokenKind>> = reservedLexemes :> IEnumerable<KeyValuePair<SyntaxLexeme, SyntaxTokenKind>>

    /// <summary>
    /// Lexemes used to group other lexemes together (ex. '(', ')', '=', ';').
    /// The start lexeme will begin the group.
    ///   - If it is null, the group will be constructed from previous sibling lexemes when the end lexeme is found. Essentially, this treats 'null' as a wildcard start.
    /// The end lexeme will finish the group.
    ///   - If it is null, the group will continue until the end of the statement or the next group.
    ///   - If it is the same as the start lexeme, the group will immediately end and the lexeme will be grouped on its own. Useful for some operators.
    /// Order is important because of first-come first-served grouping. Grouper lexemes will not be sorted by any metric, so the internal order will depend on insertion order.
    /// Group lexemes will be tokenised before grouping, and will not be grouped if part of the identifier or literal token groups.
    /// </summary>
    member this.GrouperLexemeRanges with get () : IEnumerable<SyntaxLexemeRange> = grouperLexemeRanges :> IEnumerable<SyntaxLexemeRange>

    member this.AddLexemeSeparators ([<ParamArray>] newLexemeSeparators:char[]) : unit = lexemeSeparators.AddRange newLexemeSeparators

    /// <summary>
    /// Add <paramref name="newSplitterLexeme"/> to a specific <paramref name="index"/> in the master list. Order is important because of first-come first-served splitting.
    /// New splitter lexemes will be inserted at the first position at or after <paramref name="index"/>, before the next lexeme shorter than it. This mimics length-based sorting.
    /// </summary>
    /// <param name="newSplitterLexeme">Splitter lexeme to add.</param>
    /// <param name="index"><paramref name="newSplitterLexeme"/> will be inserted at or after this index.</param>
    member this.AddSplitterLexeme (newSplitterLexeme:SyntaxLexeme, index:int) : unit =
        if splitterLexemes.Count <= index then
            splitterLexemes.Add newSplitterLexeme
        else
            if splitterLexemes.[index].Length < newSplitterLexeme.Length then
                splitterLexemes.Insert (index, newSplitterLexeme)
            else
                this.AddSplitterLexeme (newSplitterLexeme, index + 1)

    member this.AddSplitterLexemes ([<ParamArray>] newSplitterLexemes:SyntaxLexeme[]) : unit = 
        for newSplitterLexeme in newSplitterLexemes do
            this.AddSplitterLexeme (newSplitterLexeme, 0)

    /// <summary>
    /// Add <paramref name="newCombinerLexemeRange"/> to a specific <paramref name="index"/> in the master list. Position is important because of first-come first-served combining.
    /// New combiner lexemes will be inserted at the first position at or after <paramref name="index"/>, before the next lexeme shorter than it. This mimics length-based sorting.
    /// </summary>
    /// <param name="newCombinerLexemeRange">Combiner lexeme to add.</param>
    /// <param name="index"><paramref name="newCombinerLexemeRange"/> will be inserted at or after this index.</param>
    member this.AddCombinerLexemeRange (newCombinerLexemeRange:SyntaxLexemeRange, index:int) : unit =
     if combinerLexemeRanges.Count <= index then
            combinerLexemeRanges.Add newCombinerLexemeRange
        else
            if combinerLexemeRanges.[index].Start.Length < newCombinerLexemeRange.Start.Length then
                combinerLexemeRanges.Insert (index, newCombinerLexemeRange)
            else
                this.AddCombinerLexemeRange (newCombinerLexemeRange, index + 1)

    member this.AddCombinerLexemeRanges ([<ParamArray>] newCombinerLexemeRanges:SyntaxLexemeRange[]) : unit =
        for newCombinerLexemeRange in newCombinerLexemeRanges do
            this.AddCombinerLexemeRange (newCombinerLexemeRange, 0)
    
    member this.AddReservedLexeme (newReservedLexeme:(SyntaxLexeme * SyntaxTokenKind)) : unit =
        reservedLexemes.Add newReservedLexeme

    member this.AddReservedLexemes ([<ParamArray>] newReservedLexemes:(SyntaxLexeme * SyntaxTokenKind)[]) : unit = 
        for newReservedLexeme in newReservedLexemes do
            this.AddReservedLexeme newReservedLexeme
    
    member this.AddGrouperLexemeRanges ([<ParamArray>] newGrouperLexemeRanges:SyntaxLexemeRange[]) : unit = grouperLexemeRanges.AddRange newGrouperLexemeRanges

    member this.ClearLexemeSeparators () : unit = lexemeSeparators.Clear ()

    member this.ClearSplitterLexemes () : unit = splitterLexemes.Clear ()
    member this.ClearCombinerLexemeRanges () : unit = combinerLexemeRanges.Clear ()
    member this.ClearReservedLexemes () : unit = reservedLexemes.Clear ()
    member this.ClearGrouperLexemeRanges () : unit = grouperLexemeRanges.Clear ()

    /// <summary>
    /// Breaks a lexeme into one or more lexemes based on splitter lexemes.
    /// </summary>
    /// <param name="lexeme">Lexeme to split.</param>
    /// <returns>One or more split lexemes.</returns>
    abstract SplitLexeme : lexeme:SyntaxLexeme -> SyntaxLexeme[]
    override this.SplitLexeme (lexeme:SyntaxLexeme) : SyntaxLexeme[] =
        // Splitting lexemes involves two lists
        // - 'disjointLexemeList' stores all non-splitter lexemes. Nulls are used to maintain relative positions for the compositing stage in the event that two splitter lexemes are adjacent
        // - 'disjointLexemeWeaveList' stores all splitter lexemes, and will be composited with 'disjointLexemeList' in the final stage of processing
        // - 'disjointLexemeWeaveList' begins with a null value to pad it to the same length as the main list. It will be ignored during compositing
        let disjointLexemeList:LinkedList<SyntaxLexeme> = LinkedList<SyntaxLexeme> [| lexeme |]
        let disjointLexemeWeaveList:LinkedList<SyntaxLexeme> = LinkedList<SyntaxLexeme> [| null |]

        // Used in both splitting and compositing
        let mutable disjointLexemeListNode:LinkedListNode<SyntaxLexeme> = null
        let mutable disjointLexemeWeaveListNode:LinkedListNode<SyntaxLexeme> = null

        for splitterLexeme in splitterLexemes do
            // Iterate both lists in parallel
            disjointLexemeListNode <- disjointLexemeList.First
            disjointLexemeWeaveListNode <- disjointLexemeWeaveList.First

            // Loop through all lexeme nodes per splitter lexeme
            // Each iteration will determine if a lexeme needs to be split, and if so will split it into two non-splitter lexemes with a splitter lexeme 'between' them
            while disjointLexemeListNode <> null do
                let splitterLexemeIndex:int = if disjointLexemeListNode.Value <> null then disjointLexemeListNode.Value.IndexOf splitterLexeme else -1

                if splitterLexemeIndex >= 0 then
                    // Null the pre-splitter lexeme's value if the splitter lexeme begins the original lexeme
                    ignore (disjointLexemeList.AddBefore (disjointLexemeListNode, if splitterLexemeIndex <> 0 then disjointLexemeListNode.Value.Substring (0, splitterLexemeIndex) else null))

                    // Add the splitter lexeme to the composition 'weave' list
                    ignore (disjointLexemeWeaveList.AddAfter (disjointLexemeWeaveListNode, disjointLexemeListNode.Value.Substring (splitterLexemeIndex, splitterLexeme.Length)))

                    // Null the post-splitter lexeme's value if the splitter lexeme ends the original lexeme
                    ignore (disjointLexemeList.AddAfter (disjointLexemeListNode, if splitterLexemeIndex <> (disjointLexemeListNode.Value.Length - 1) then disjointLexemeListNode.Value.Substring (splitterLexemeIndex + splitterLexeme.Length, disjointLexemeListNode.Value.Length - (splitterLexemeIndex + splitterLexeme.Length)) else null))

                    let oldNode:LinkedListNode<SyntaxLexeme> = disjointLexemeListNode
                    
                    // Remove the original node now that it has been split
                    disjointLexemeListNode <- disjointLexemeListNode.Previous
                    disjointLexemeList.Remove oldNode
                
                disjointLexemeListNode <- disjointLexemeListNode.Next
                disjointLexemeWeaveListNode <- disjointLexemeWeaveListNode.Next

        // Composition list
        let compositedLexemesList:List<SyntaxLexeme> = List<SyntaxLexeme> (disjointLexemeList.Count + disjointLexemeWeaveList.Count)
        
        disjointLexemeListNode <- disjointLexemeList.First
        disjointLexemeWeaveListNode <- disjointLexemeWeaveList.First

        // There will always be one more non-splitter lexeme than splitter lexeme
        // The first lexeme in the weave list is ignored, as it was null and only there for padding
        if disjointLexemeListNode.Value <> null then
            compositedLexemesList.Add disjointLexemeListNode.Value

        disjointLexemeListNode <- disjointLexemeListNode.Next
        disjointLexemeWeaveListNode <- disjointLexemeWeaveListNode.Next
        
        if disjointLexemeWeaveList.Count > 0 then
            // Alternate picking of lexemes from splitters and non-splitters
            while disjointLexemeListNode <> null do
                compositedLexemesList.Add disjointLexemeWeaveListNode.Value

                if disjointLexemeListNode.Value <> null then
                    compositedLexemesList.Add disjointLexemeListNode.Value

                disjointLexemeListNode <- disjointLexemeListNode.Next
                disjointLexemeWeaveListNode <- disjointLexemeWeaveListNode.Next

        compositedLexemesList.ToArray ()

    /// <summary>
    /// Transforms a lexeme into a token.
    /// </summary>
    /// <param name="lexeme">Lexeme to transform.</param>
    /// <returns>Transformed token.</returns>
    abstract TransformLexeme : lexeme:SyntaxLexeme -> SyntaxToken
    override this.TransformLexeme (lexeme:SyntaxLexeme) : SyntaxToken = 
        let mutable tokenKind:SyntaxTokenKind = SyntaxTokenKind.IdentifierOrLiteral

        // Check reserved lexeme list for token kind other than the default
        ignore (reservedLexemes.TryGetValue (lexeme, &tokenKind))

        SyntaxToken (lexeme, tokenKind)

    /// <summary>
    /// Constructs a heirarchy of tokens based on grouper tokens.
    /// </summary>
    /// <param name="tokens">Sibling tokens, usually an entire unit of related tokens (a file or distinct block).</param>
    /// <returns>Heirarchy of input tokens that represents nested priority groups.</returns>
    abstract member GroupTokens : tokens:SyntaxToken[] -> SyntaxTokenHeirarchy
    override this.GroupTokens (tokens:SyntaxToken[]) : SyntaxTokenHeirarchy =
        // Helper function to find occurance of token lexeme after an index in an array
        let rec FindToken (tokens:SyntaxToken[], startTokenIndex:int, endTokenIndex:int, target:SyntaxLexeme) : int =
            if startTokenIndex >= endTokenIndex then
                -1
            else
                if tokens.[startTokenIndex].Kind <> SyntaxTokenKind.IdentifierOrLiteral && tokens.[startTokenIndex].Lexeme = target then
                    startTokenIndex
                else
                    FindToken (tokens, startTokenIndex + 1, endTokenIndex, target)

        // Helper function that constructs an array with an additional first and last value
        let WrapArray (first:'TYPE, middle:'TYPE[], last:'TYPE) : 'TYPE[] =
            let returnValue:'TYPE[] = Array.zeroCreate (middle.Length + 2)

            returnValue.[0] <- first

            for index = 0 to middle.Length - 1 do
                returnValue.[index + 1] <- middle.[index]

            returnValue.[returnValue.Length - 1] <- last

            returnValue

        // Recursively (depth-first) constructs token heirarchy
        let rec BuildHeirarchy (tokens:SyntaxToken[], startTokenIndex:int, endTokenIndex:int) : SyntaxTokenHeirarchy =
            // Will be used to construct a 'parent' heirarchy
            let childHeirarchies:List<SyntaxTokenHeirarchy> = List<SyntaxTokenHeirarchy> ()

            // Counts the number of times the child heirarchy list has been 'compacted'.
            // 'Compaction' means stripping back to the previous compaction, compositing the loose children into a single child node, and adding the result back to the list.
            let mutable childHeirarchyCompactionCount:int = 0
                
            let mutable tokenIndex:int = startTokenIndex

            // A while loop allows the token index to be moved up past child token blocks that are processed by a deeper invocation
            while tokenIndex < endTokenIndex do
                let token:SyntaxToken = tokens.[tokenIndex];

                // Add token individually to this parent, meaning it will be a leaf
                let mutable addTokenAsLeaf = true
                
                // Grouper tokens cannot be an identifier or a literal
                if token.Kind <> SyntaxTokenKind.IdentifierOrLiteral then
                    let mutable grouperLexemeRangeIndex = 0;
                        
                    // A while loop allows the group check to be terminated once one is found
                    while grouperLexemeRangeIndex < grouperLexemeRanges.Count do
                        let grouperLexemeRange:SyntaxLexemeRange = grouperLexemeRanges.[grouperLexemeRangeIndex]

                        // Allow late grouping (compaction) for wildcards
                        if grouperLexemeRange.Start = null then
                            if token.Lexeme = grouperLexemeRange.End then
                                addTokenAsLeaf <- false

                                if not grouperLexemeRange.DiscardStartAndEnd then
                                    childHeirarchies.Add (SyntaxTokenHeirarchy.Leaf token)
                                    
                                if childHeirarchies.Count - childHeirarchyCompactionCount > 0 then
                                    // Perform compaction back to the previous compaction point (don't compact compacted children)
                                    let compactedChild:SyntaxTokenHeirarchy = SyntaxTokenHeirarchy.Parent ((childHeirarchies.Skip childHeirarchyCompactionCount).ToArray ())

                                    // Roll back to compaction point in preparation to add compacted children
                                    childHeirarchies.RemoveRange (childHeirarchyCompactionCount, childHeirarchies.Count - childHeirarchyCompactionCount)

                                    childHeirarchies.Add compactedChild

                                    // Track the number of compactions that have occured, so compacted children aren't further compacted
                                    childHeirarchyCompactionCount <- childHeirarchyCompactionCount + 1
                        elif token.Lexeme = grouperLexemeRange.Start then
                            // Terminate group check
                            grouperLexemeRangeIndex <- grouperLexemeRanges.Count - 1
                                
                            // The token will be handled as a block in a deeper invocation, so it no longer needs to be added individually
                            addTokenAsLeaf <- false

                            // Find the group end lexeme if it's provided
                            let groupEndLexemeIndex:int = if grouperLexemeRange.End <> null then FindToken (tokens, tokenIndex, endTokenIndex, grouperLexemeRange.End) else -1

                            // If the group lexeme range has an end lexeme, find it and group to that point
                            if groupEndLexemeIndex >= 0 then
                                // If the end lexeme is not the same as the start lexeme, group to the end lexeme index
                                if groupEndLexemeIndex > tokenIndex then
                                    // If group lexemes should be discarded, ignore their tokens
                                    if grouperLexemeRange.DiscardStartAndEnd then
                                        // Ensure the group block is not empty
                                        if groupEndLexemeIndex > tokenIndex + 1 then 
                                            childHeirarchies.Add (BuildHeirarchy (tokens, tokenIndex + 1, groupEndLexemeIndex))
                                    else
                                        // Ensure the group block is not empty
                                        if groupEndLexemeIndex > tokenIndex + 1 then 
                                            // Wrap the group in the start and end tokens
                                            childHeirarchies.Add (SyntaxTokenHeirarchy.Parent (WrapArray (SyntaxTokenHeirarchy.Leaf token, (BuildHeirarchy (tokens, tokenIndex + 1, groupEndLexemeIndex)).GetComponentHeirarchies (), SyntaxTokenHeirarchy.Leaf tokens.[groupEndLexemeIndex])))
                                        else
                                            childHeirarchies.Add (SyntaxTokenHeirarchy.Parent [| SyntaxTokenHeirarchy.Leaf token; SyntaxTokenHeirarchy.Leaf tokens.[groupEndLexemeIndex] |])

                                    // Skip tokens up to the end of the group
                                    tokenIndex <- groupEndLexemeIndex
                                elif not grouperLexemeRange.DiscardStartAndEnd then
                                    childHeirarchies.Add (SyntaxTokenHeirarchy.Parent [| SyntaxTokenHeirarchy.Leaf token |])
                            else
                                // If group lexemes should be discarded, ignored their tokens
                                if grouperLexemeRange.DiscardStartAndEnd then
                                    // Ensure the group block is not empty
                                    if tokenIndex < endTokenIndex then
                                        childHeirarchies.Add (BuildHeirarchy (tokens, tokenIndex + 1, endTokenIndex))
                                else
                                    // Ensure the group block is not empty
                                    if tokenIndex < endTokenIndex then
                                        childHeirarchies.Add (SyntaxTokenHeirarchy.Parent (Array.append<SyntaxTokenHeirarchy> [| SyntaxTokenHeirarchy.Leaf token |] ((BuildHeirarchy (tokens, tokenIndex + 1, endTokenIndex)).GetComponentHeirarchies ())))
                                    else
                                        childHeirarchies.Add (SyntaxTokenHeirarchy.Parent [| SyntaxTokenHeirarchy.Leaf token |])

                                // Skip all tokens remaining in the block
                                tokenIndex <- endTokenIndex - 1

                        // Prepare to check the next group
                        grouperLexemeRangeIndex <- grouperLexemeRangeIndex + 1

                // If the token was not handled as a block, add it as a leaf
                if addTokenAsLeaf then
                    childHeirarchies.Add (SyntaxTokenHeirarchy.Leaf token)

                // Prepare to check the next group
                tokenIndex <- tokenIndex + 1

            SyntaxTokenHeirarchy.Parent (childHeirarchies.ToArray ())
        
        BuildHeirarchy (tokens, 0, tokens.Length)
        
    /// <summary>
    /// Tokenizes a block of source code with the rules assigned to this lexer.
    /// </summary>
    /// <param name="source">Source code to tokenise.</param>
    /// <returns>Heirarchy of source tokens.</returns>
    member this.ProcessSource (source:string) : SyntaxTokenHeirarchy =
        let tokens:List<SyntaxToken> = List<SyntaxToken> ()

        // Process range of characters that represent a lexeme into one more actual tokens
        let ProcessLexeme (sourceIndex:int, length:int, performSplitting:bool) : unit =
            if length > 0 then
                let lexeme:SyntaxLexeme = (source.Substring (sourceIndex, length))
                
                // Splitting
                let splitLexemes:SyntaxLexeme[] = if performSplitting then this.SplitLexeme lexeme else [| lexeme |]
                
                // Transformation
                let transformedTokens:SyntaxToken[] = Array.create<SyntaxToken> splitLexemes.Length null

                for splitLexemeIndex = 0 to splitLexemes.Length - 1 do
                    transformedTokens.[splitLexemeIndex] <- this.TransformLexeme splitLexemes.[splitLexemeIndex]
                
                tokens.AddRange transformedTokens

        // Starting index of the current lexeme to process
        let mutable currentLexemeIndex:int = 0

        // Character index in the source to process
        let mutable currentCharacterIndex:int = 0

        // Process the current lexeme characters (up to the current, but not including) and store the index for the next lexeme
        let ConsumeCurrentLexeme (includeCurrentCharacter:bool, performSplitting:bool) : unit =
            if includeCurrentCharacter then
                ProcessLexeme (currentLexemeIndex, currentCharacterIndex - currentLexemeIndex + 1, performSplitting)
            else
                ProcessLexeme (currentLexemeIndex, currentCharacterIndex - currentLexemeIndex, performSplitting)

            currentLexemeIndex <- currentCharacterIndex + 1
        
        // Track whether we are combining lexemes, and record progress through combining
        let mutable currentCombinerLexemeRange:Option<SyntaxLexemeRange> = None
        let mutable currentCombinerLexemeProgress:int = 0
        
        // Each charater in the source is iterated over individually
        while currentCharacterIndex < source.Length do
            let currentCharacter:char = source.[currentCharacterIndex]
            
            // Different path when combining lexemes
            match currentCombinerLexemeRange with
            | Some (currentCombinerLexemeRangeValue) -> 
                // Each iteration progresses through the end combiner lexeme rather than checking outright for a match
                if currentCharacter = currentCombinerLexemeRangeValue.End.[currentCombinerLexemeProgress] then
                    if currentCombinerLexemeProgress = currentCombinerLexemeRangeValue.End.Length - 1 then
                        if currentCombinerLexemeRangeValue.DiscardStartAndEnd then
                            currentLexemeIndex <- currentLexemeIndex + 1
                        
                        ConsumeCurrentLexeme (not currentCombinerLexemeRangeValue.DiscardStartAndEnd, false)
                            
                        // Stop combining
                        currentCombinerLexemeRange <- None
                        currentCombinerLexemeProgress <- 0
                    else
                        // Progress combining
                        currentCombinerLexemeProgress <- currentCombinerLexemeProgress + 1
                else
                    // Reset combining
                    currentCombinerLexemeProgress <- 0
            | None ->
                // Find matching combiner lexeme, if one exists
                let rec MatchCombinerLexemeRange (searchIndex:int) : Option<SyntaxLexemeRange> =
                    if searchIndex >= combinerLexemeRanges.Count then
                        None
                    else
                        let combinerLexemeRange:SyntaxLexemeRange = combinerLexemeRanges.[searchIndex]
                        
                        if currentCharacter = combinerLexemeRange.Start.[0] && SyntaxLexer.MatchTextAgainstSource (combinerLexemeRange.Start, source, currentCharacterIndex) then
                            Some (combinerLexemeRange)
                        else
                            MatchCombinerLexemeRange (searchIndex + 1)

                match MatchCombinerLexemeRange 0 with
                | Some (matchedCombinerLexemeRange) ->
                    // Step back and consume characters to this point, starting a clean slate before the combiner lexeme
                    currentCharacterIndex <- currentCharacterIndex - 1

                    ConsumeCurrentLexeme (true, true)

                    // Step to the end of the combiner start lexeme
                    currentCharacterIndex <- currentCharacterIndex + matchedCombinerLexemeRange.Start.Length

                    // Begin combining
                    currentCombinerLexemeRange <- Some matchedCombinerLexemeRange
                | None -> 
                    match currentCharacter with
                    | _ when lexemeSeparators.Contains currentCharacter ->
                        ConsumeCurrentLexeme (false, true)
                    | _ -> ignore currentCharacter

            currentCharacterIndex <- currentCharacterIndex + 1
        
        // Ensure no loose lexemes slipped through
        ConsumeCurrentLexeme (false, currentCombinerLexemeRange = None)

        // Contruct heirarchy before returning
        this.GroupTokens (tokens.ToArray ())