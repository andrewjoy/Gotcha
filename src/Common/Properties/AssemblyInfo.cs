﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Gotcha.Common")]
[assembly: AssemblyDescription("Gotcha general purpose types.")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

[assembly: Guid("b658ba0f-fd52-4369-b757-aae9bcffac67")]

[assembly: AssemblyVersion("0.1.0.0")]
[assembly: AssemblyFileVersion("0.1.0.0")]
